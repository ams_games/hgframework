#pragma once

// TinySTL includes (https://github.com/mendsley/tinystl)
#include <TINYSTL\buffer.h>
#include <TINYSTL\hash.h>
#include <TINYSTL\string.h>
#include <TINYSTL\unordered_map.h>
#include <TINYSTL\unordered_set.h>
#include <TINYSTL\vector.h>

// Ordered containers
#include <set>
#include <map>