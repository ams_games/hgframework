#pragma once

#include "HGGraphics.h"

namespace hg
{
	namespace Graphics
	{
		class CommandRecorder
		{
		public:
			virtual void Reset() = 0;
			virtual void Close() = 0;

			virtual void ExecuteCommandBundles(unsigned int nCmdBundles, CommandBundle *const *ppCommandBundles) = 0;

			virtual void BeginRenderPass(const RenderPass * pRenderPass) = 0;
			virtual void BeginMainRenderPass(const RenderPass * pRenderPass, const UINT currentFramebufferIdx) = 0;
			virtual void AdvanceSubpass() = 0;
			virtual void EndRenderPass() = 0;

			virtual void SetPipelineLayout(const PipelineLayout * pPipelineLayout) = 0;
			virtual void SetPipelineState(const PipelineState * pPipelineState) = 0;

			virtual void SetPrimitiveTopology(PrimitiveTopology primitiveTopology) = 0;
			virtual void SetVertexBuffers(UINT start, UINT nViews, Buffer * const * ppBuffers) = 0;
			virtual void SetIndexBuffer(Buffer* pBuffer) = 0;

			virtual void SetScissorRects(UINT nRects, const Rect * pRects) = 0;
			virtual void SetViewports(UINT nViewports, const Viewport * pViewports) = 0;

			virtual void SetBlendFactor(const FLOAT blendFactors[4]) = 0;
			virtual void SetStencilRefValue(UINT stencilRefValue) = 0;

			virtual void Draw(const UINT& nVertices, const UINT& nInstances, const UINT& vertexStart, const UINT& instanceStart) = 0;
			virtual void DrawIndexed(const UINT& nIndices, const UINT& nInstances, const UINT& indexStart, const UINT& vertexOffset, const UINT& instanceStart) = 0;
		};

		class CommandBundle
		{
		public:
			virtual void Reset() = 0;
			virtual void Close() = 0;

			virtual void SetPipelineLayout(const PipelineLayout * pPipelineLayout) = 0;
			virtual void SetPipelineState(const PipelineState * pPipelineState) = 0;

			virtual void SetPrimitiveTopology(PrimitiveTopology primitiveTopology) = 0;
			virtual void SetVertexBuffers(UINT start, UINT nViews, Buffer * const * ppBuffers) = 0;

			virtual void SetBlendFactor(const FLOAT blendFactors[4]) = 0;
			virtual void SetStencilRefValue(UINT stencilRefValue) = 0;

			virtual void DrawInstanced(UINT verticesPerInstance, UINT nInstances, UINT vertexStart, UINT instanceStart) = 0;
		};
	}
}