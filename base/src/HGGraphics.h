#pragma once

#include "HGPlatform.h"
#include "HGContainers.h"

// DEFINES
#define BACKBUFFER_COUNT 2
#define MAIN_RENDERPASS_IDX ~0u
#define SHADER_STAGES_COUNT 6
#define RESOURCE_FREQUENCY_COUNT 5

namespace hg 
{
	namespace Graphics
	{
		// ENUMS

		// TODO : Remove
		enum CommandRecorderType
		{
			CRT_DIRECT = 0 << 0,
			CRT_COMPUTE = 1 << 0,
			CRT_COPY = 1 << 1
		};

		enum PrimitiveTopology
		{
			PT_UNDEFINED = 0,
			PT_POINTLIST = 1,
			PT_LINELIST = 2,
			PT_LINESTRIP = 3,
			PT_TRIANGLELIST = 4,
			PT_TRIANGLESTRIP = 5,
			PT_LINELIST_ADJ = 10,
			PT_LINESTRIP_ADJ = 11,
			PT_TRIANGLELIST_ADJ = 12,
			PT_TRIANGLESTRIP_ADJ = 13,
			PT_CONTROL_POINT_PATCHLIST_1 = 33,
			PT_CONTROL_POINT_PATCHLIST_2 = 34,
			PT_CONTROL_POINT_PATCHLIST_3 = 35,
			PT_CONTROL_POINT_PATCHLIST_4 = 36,
			PT_CONTROL_POINT_PATCHLIST_5 = 37,
			PT_CONTROL_POINT_PATCHLIST_6 = 38,
			PT_CONTROL_POINT_PATCHLIST_7 = 39,
			PT_CONTROL_POINT_PATCHLIST_8 = 40,
			PT_CONTROL_POINT_PATCHLIST_9 = 41,
			PT_CONTROL_POINT_PATCHLIST_10 = 42,
			PT_CONTROL_POINT_PATCHLIST_11 = 43,
			PT_CONTROL_POINT_PATCHLIST_12 = 44,
			PT_CONTROL_POINT_PATCHLIST_13 = 45,
			PT_CONTROL_POINT_PATCHLIST_14 = 46,
			PT_CONTROL_POINT_PATCHLIST_15 = 47,
			PT_CONTROL_POINT_PATCHLIST_16 = 48,
			PT_CONTROL_POINT_PATCHLIST_17 = 49,
			PT_CONTROL_POINT_PATCHLIST_18 = 50,
			PT_CONTROL_POINT_PATCHLIST_19 = 51,
			PT_CONTROL_POINT_PATCHLIST_20 = 52,
			PT_CONTROL_POINT_PATCHLIST_21 = 53,
			PT_CONTROL_POINT_PATCHLIST_22 = 54,
			PT_CONTROL_POINT_PATCHLIST_23 = 55,
			PT_CONTROL_POINT_PATCHLIST_24 = 56,
			PT_CONTROL_POINT_PATCHLIST_25 = 57,
			PT_CONTROL_POINT_PATCHLIST_26 = 58,
			PT_CONTROL_POINT_PATCHLIST_27 = 59,
			PT_CONTROL_POINT_PATCHLIST_28 = 60,
			PT_CONTROL_POINT_PATCHLIST_29 = 61,
			PT_CONTROL_POINT_PATCHLIST_30 = 62,
			PT_CONTROL_POINT_PATCHLIST_31 = 63,
			PT_CONTROL_POINT_PATCHLIST_32 = 64
		};

		enum SampleCount
		{
			SC_1	= 1 << 0,
			SC_2	= 1 << 1,
			SC_4	= 1 << 2,
			SC_8	= 1 << 3,
			SC_16	= 1 << 4,
			SC_32	= 1 << 5,
			SC_64	= 1 << 6,
		};

		enum PipelineType
		{
			PT_GRAPHICS	= 0,
			PT_COMPUTE		= 1
		};

		enum FilteringMode
		{
			POINT,
			LINEAR,
			ANISOTROPIC
		};

		enum CompareOp
		{
			NEVER,
			LESS,
			EQUAL,
			LESS_OR_EQUAL,
			GREATER,
			NOT_EQUAL,
			GREATER_OR_EQUAL,
			ALWAYS
		};

		enum TextureBorderColor
		{
			FLOAT_TRANSPARENT_BLACK,
			INT_TRANSPARENT_BLACK,
			FLOAT_OPAQUE_BLACK,
			INT_OPAQUE_BLACK,
			FLOAT_OPAQUE_WHITE,
			INT_OPAQUE_WHITE
		};

		enum TextureAddressMode
		{
			REPEAT,
			MIRROR,
			CLAMP,
			BORDER,
			MIRROR_CLAMP
		};

		enum BufferUsage
		{
			BU_COPY_SRC = 0x00000001,
			BU_COPY_DST = 0x00000002,
			BU_UNIFORM_TEXEL_BUFFER = 0x00000004,
			BU_STORAGE_TEXEL_BUFFER = 0x00000008,
			BU_UNIFORM_BUFFER = 0x00000010,
			BU_STORAGE_BUFFER = 0x00000020,
			BU_INDEX_BUFFER = 0x00000040,
			BU_VERTEX_BUFFER = 0x00000080,
			BU_INDIRECT_BUFFER = 0x00000100
		};

		enum TextureDimensions
		{
			TD_1D,
			TD_2D,
			TD_3D,
			TD_CUBE
		};

		enum ResourceUpdateFrequency
		{
			STATIC,
			PER_RESIZE,
			PER_FRAME,
			PER_BATCH,
			PER_DRAW
		};

		enum CullMode
		{
			NONE,
			FRONT,
			BACK,
		};

		// STRUCTS

		struct Rect;
		struct Viewport;

		// INTERFACES
		// TODO : Slowly move these into their own headers.

		//|--------((HG))-------|--------------------((DX12))-------------------|------------------((VK))--------------------|
		//| * Shader			| * Shader Loader + Reflection Info				| * Shader Loader + Reflection Info			 |
		//| * Buffer			| * Resource + BufferViews(CBV//VBV//IBV//OBV)+	| * Buffer + BufferView + DeviceMemory +	 |
		//|						|	 ResourceBarrier Manager					|	 ResourceBarrier Manager				 |
		//| * Texture			| * Resource + ImageViews(RTV//SRV//UAV)+		| * Image + ImageView + DeviceMemory +		 |
		//|						|	 ResourceBarrier Manager					|	 ResourceBarrier Manager				 |
		//| * RenderPass		| * RTV ResBarrier Manager						| * RenderPass								 |
		//| * PipelineLayout	| * RootSignature								| * PipelineLayout							 |
		//| * PipelineState		| * PipelineState								| * Pipeline								 |
		//| * CommandRecorder	| * CommandList + Ref to cmdAllocator//cmdQueue	| * CommandBuffer + Ref to cmdPool//cmdQueue |
		//| * CommandBundle		| * Bundle + Ref to cmdAllocator				| * Secondary CommandBuffer + Ref to cmdPool |
		//| * Renderer			| * Device + Queue Manager + Swapchain +		| * Device + Queue Manager + Swapchain +	 |
		//|						|	 Helper functions							|	 Helper functions						 |
		//|---------------------|-----------------------------------------------|--------------------------------------------|

		class Shader;
		class Buffer;
		class Texture;
		class Sampler;
		class RenderTarget;
		class RenderSubpass;
		class RenderPass;
		class PipelineLayout;
		class PipelineState;
		class CommandRecorder;
		class CommandBundle;
		class Renderer;
	}
}