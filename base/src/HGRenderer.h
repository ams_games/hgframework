#pragma once

#include "HGGraphics.h"

namespace hg
{
	namespace Graphics
	{
		class Renderer
		{
		public:
			virtual Shader* LoadShader(
				const LPWSTR& vertexShaderName,
				const LPWSTR& tessControlShaderName,
				const LPWSTR& tessEvaluationShaderName,
				const LPWSTR& geometryShaderName,
				const LPWSTR& fragmentShaderName,
				const LPWSTR& computeShaderName) = 0;
			virtual void DestroyShader(Shader* pShader) = 0;
			virtual Texture* LoadTexture(const LPWSTR& textureName) = 0;
			virtual void DestroyTexture(Texture* pTexture) = 0;
			virtual Buffer* CreateBuffer(const UINT& bufferSize, const UINT& elementStride, const UINT& bufferUsageFlags, const void* pData = nullptr) = 0;
			virtual void UpdateBuffer(Buffer const* pBuffer, void const* pData) = 0;
			virtual void DestroyBuffer(Buffer* pBuffer) = 0;
			virtual RenderTarget* CreateRenderTarget(const UINT& renderPassIdx,
				const UINT& width, const UINT& height, 
				const UINT& format, const float clearColor[4],
				const SampleCount& samples = SampleCount::SC_1, const TextureDimensions& dimensions = TextureDimensions::TD_2D,
				const UINT& layers = 1, const UINT& mips = 1, const bool& sampled = false) = 0;
			virtual void DestroyRenderTarget(RenderTarget* pRenderTarget) = 0;
			virtual Sampler* CreateSampler(
				const FilteringMode& minFiltering, const FilteringMode& magFiltering, const FilteringMode& mipFiltering,
				const TextureAddressMode& modeU, const TextureAddressMode& modeV, const TextureAddressMode& modeW,
				const UINT& maxAnisotropy = 1u, const TextureBorderColor& borderColor = FLOAT_OPAQUE_BLACK, const float& mipLODBias = 1.0f,
				const float& minLOD = 0.0f, const float& maxLOD = ~0u, const CompareOp& compareOp = NEVER) = 0;
			virtual void DestroySampler(Sampler* pSampler) = 0;
			virtual RenderSubpass* CreateRenderSubpass(const PipelineType& type, const UINT& subpassIdx,
				const UINT& nInputRTs, RenderTarget* const* ppInputRTs,
				const UINT& nOutputRTs, RenderTarget* const* ppOutputRTs, RenderTarget* const* ppResolveRTs,
				const bool& depthWrite, RenderTarget const* pDepthBuffer,
				const UINT& nPreservedRTs, RenderTarget* const* ppPreservedRTs,
				const UINT& nDependencies, UINT const* ppDependencies) = 0;
			virtual void DestroyRenderSubpass(RenderSubpass* pRenderSubpass) = 0;
			virtual RenderPass* GetMainRenderPass() = 0;
			virtual RenderPass* CreateRenderPass(const UINT& width, const UINT& height, const float framebufferClearColor[4], const UINT& nSubpasses = 0, RenderSubpass* const * ppSubpasses = nullptr) = 0;
			virtual void DestroyRenderPass(RenderPass* pRenderPass) = 0;
			virtual PipelineLayout* CreatePipelineLayout(const Shader* pShader) = 0;
			virtual void DestroyPipelineLayout(PipelineLayout* pPipelineLayout) = 0;
			virtual PipelineState* CreatePipelineState(const Shader* pShader, const PipelineLayout* pPipelineLayout, const RenderPass* pRenderPass, const UINT& subpassIdx, const CullMode& cullMode = NONE) = 0;
			virtual void DestroyPipelineState(PipelineState* pPipelineState) = 0;
			virtual CommandRecorder* CreateCommandRecorder(const CommandRecorderType& type) = 0;
			virtual void DestroyCommandRecorder(CommandRecorder* pCommandRecorder) = 0;
			virtual CommandBundle* CreateCommandBundle(const RenderPass* pRenderPass, const UINT& subpassIdx) = 0;
			virtual void DestroyCommandBundle(CommandBundle* pCommandBundle) = 0;

			virtual void BindTexture(const tinystl::string& name, Texture * pTexture, Sampler * pSampler, PipelineLayout* pPipelineLayout) = 0;
			// TODO : Remove this.
			virtual void BindTexture(const tinystl::string& name, RenderTarget * pTexture, Sampler * pSampler, PipelineLayout* pPipelineLayout) = 0;
			virtual void BindSampler(const tinystl::string& name, Sampler * pSampler, PipelineLayout* pPipelineLayout) = 0;
			virtual void BindBuffer(const tinystl::string& name, Buffer* pBuffer, PipelineLayout* pPipelineLayout) = 0;

			virtual void ExecuteCommands(unsigned int nCmdRecorders, CommandRecorder* const* ppCommandRecorders) = 0;
			virtual void FinishFrame() = 0;
			virtual void Present() = 0;

			unsigned int GetMainFramebufferIdx() { return m_mainFramebufferIdx; };

			virtual void ResizeMainFramebuffer() = 0;

		protected:
			unsigned int m_mainFramebufferIdx;
		};
	}
}