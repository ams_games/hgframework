#pragma once

namespace hg
{
	namespace FileSystem
	{
		enum AssetType
		{
			DEFAULT = 0 << 0,
			SHADER	= 1 << 0,
			TEXTURE = 1 << 1,
			MODEL	= 1 << 2
		};

		//const WCHAR* assetsPath = L"\\assets\\";
		//const size_t assetsPathLength = sizeof(assetsPath) * sizeof(WCHAR);
		//const WCHAR* shadersPath = L"\\assets\\shaders\\";
		//const size_t shadersPathLength = sizeof(shadersPath) * sizeof(WCHAR);
		//const WCHAR* texturesPath = L"\\assets\\textures\\";
		//const size_t texturesPathLength = sizeof(texturesPath) * sizeof(WCHAR);

		inline void GetApplicationPath(_Out_writes_(pathSize) WCHAR* path, UINT pathSize)
		{
			if (path == nullptr)
			{
				throw std::exception();
			}

			DWORD size = GetModuleFileName(nullptr, path, pathSize);
			if (size == 0 || size == pathSize)
			{
				// Method failed or path was truncated.
				throw std::exception();
			}

			WCHAR* lastSlash = wcsrchr(path, L'\\');
			if (lastSlash)
			{
				*(lastSlash + 1) = L'\0';
			}
		}

		inline void GetAssetsPath(_Out_writes_(pathSize) WCHAR* path, UINT pathSize)
		{
			const WCHAR* assetsPath = L"\\assets\\";
			const size_t assetsPathLength = sizeof(L"\\assets\\") / _WCHAR_T_SIZE;

			if (path == nullptr)
			{
				throw std::exception();
			}

			DWORD size = GetModuleFileName(nullptr, path, pathSize);
			if (size == 0 || size == pathSize)
			{
				// Method failed or path was truncated.
				throw std::exception();
			}

			WCHAR* buildPath = wcsstr(path, L"\\build");
			if (buildPath)
			{
				wcscpy_s(buildPath, assetsPathLength ,assetsPath);
			}
		}

		inline void GetShadersPath(_Out_writes_(pathSize) WCHAR* path, UINT pathSize)
		{
			const WCHAR* shadersPath = L"\\assets\\shaders\\";
			const size_t shadersPathLength = sizeof(L"\\assets\\shaders\\") / _WCHAR_T_SIZE;

			if (path == nullptr)
			{
				throw std::exception();
			}

			DWORD size = GetModuleFileName(nullptr, path, pathSize);
			if (size == 0 || size == pathSize)
			{
				// Method failed or path was truncated.
				throw std::exception();
			}

			WCHAR* buildPath = wcsstr(path, L"\\build");
			if (buildPath)
			{
				wcscpy_s(buildPath, shadersPathLength, shadersPath);
			}
		}

		inline void GetTexturesPath(_Out_writes_(pathSize) WCHAR* path, UINT pathSize)
		{
			const WCHAR* texturesPath = L"\\assets\\textures\\";
			const size_t texturesPathLength = sizeof(L"\\assets\\textures\\") / _WCHAR_T_SIZE;

			if (path == nullptr)
			{
				throw std::exception();
			}

			DWORD size = GetModuleFileName(nullptr, path, pathSize);
			if (size == 0 || size == pathSize)
			{
				// Method failed or path was truncated.
				throw std::exception();
			}

			WCHAR* buildPath = wcsstr(path, L"\\build");
			if (buildPath)
			{
				wcscpy_s(buildPath, texturesPathLength, texturesPath);
			}
		}

		inline void GetModelsPath(_Out_writes_(pathSize) WCHAR* path, UINT pathSize)
		{
			const WCHAR* modelsPath = L"\\assets\\models\\";
			const size_t modelsPathLength = sizeof(L"\\assets\\models\\") / _WCHAR_T_SIZE;

			if (path == nullptr)
			{
				throw std::exception();
			}

			DWORD size = GetModuleFileName(nullptr, path, pathSize);
			if (size == 0 || size == pathSize)
			{
				// Method failed or path was truncated.
				throw std::exception();
			}

			WCHAR* buildPath = wcsstr(path, L"\\build");
			if (buildPath)
			{
				wcscpy_s(buildPath, modelsPathLength, modelsPath);
			}
		}
	}
}