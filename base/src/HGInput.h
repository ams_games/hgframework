#pragma once

namespace hg
{
	namespace Input
	{
		enum MouseButton
		{
			LEFT_BUTTON,
			RIGHT_BUTTON
		};

#if defined(_WIN32)
		enum KeyCode
		{
			KEY_INVALID = 0,

			KEY_0 = int('0'),
			KEY_1 = int('1'),
			KEY_2 = int('2'),
			KEY_3 = int('3'),
			KEY_4 = int('4'),
			KEY_5 = int('5'),
			KEY_6 = int('6'),
			KEY_7 = int('7'),
			KEY_8 = int('8'),
			KEY_9 = int('9'),

			KEY_A = int('A'),
			KEY_B = int('B'),
			KEY_C = int('C'),
			KEY_D = int('D'),
			KEY_E = int('E'),
			KEY_F = int('F'),
			KEY_G = int('G'),
			KEY_H = int('H'),
			KEY_I = int('I'),
			KEY_J = int('J'),
			KEY_K = int('K'),
			KEY_L = int('L'),
			KEY_M = int('M'),
			KEY_N = int('N'),
			KEY_O = int('O'),
			KEY_P = int('P'),
			KEY_Q = int('Q'),
			KEY_R = int('R'),
			KEY_S = int('S'),
			KEY_T = int('T'),
			KEY_U = int('U'),
			KEY_V = int('V'),
			KEY_W = int('W'),
			KEY_X = int('X'),
			KEY_Y = int('Y'),
			KEY_Z = int('Z'),
			KEY_COMMA = int(','),
			KEY_PERIOD = int('.'),

			KEY_LEFT = VK_LEFT,
			KEY_RIGHT = VK_RIGHT,
			KEY_UP = VK_UP,
			KEY_DOWN = VK_DOWN,
			KEY_CTRL = VK_CONTROL,
			KEY_SHIFT = VK_SHIFT,
			KEY_ENTER = VK_RETURN,
			KEY_SPACE = VK_SPACE,
			KEY_TAB = VK_TAB,
			KEY_ESCAPE = VK_ESCAPE,
			KEY_BACKSPACE = VK_BACK,
			KEY_HOME = VK_HOME,
			KEY_END = VK_END,
			KEY_INSERT = VK_INSERT,
			KEY_DELETE = VK_DELETE,
			KEY_CAPITAL = VK_CAPITAL,
			KEY_ALT = VK_MENU,

			KEY_F1 = VK_F1,
			KEY_F2 = VK_F2,
			KEY_F3 = VK_F3,
			KEY_F4 = VK_F4,
			KEY_F5 = VK_F5,
			KEY_F6 = VK_F6,
			KEY_F7 = VK_F7,
			KEY_F8 = VK_F8,
			KEY_F9 = VK_F9,
			KEY_F10 = VK_F10,
			KEY_F11 = VK_F11,
			KEY_F12 = VK_F12,

			KEY_NUMPAD0 = VK_NUMPAD0,
			KEY_NUMPAD1 = VK_NUMPAD1,
			KEY_NUMPAD2 = VK_NUMPAD2,
			KEY_NUMPAD3 = VK_NUMPAD3,
			KEY_NUMPAD4 = VK_NUMPAD4,
			KEY_NUMPAD5 = VK_NUMPAD5,
			KEY_NUMPAD6 = VK_NUMPAD6,
			KEY_NUMPAD7 = VK_NUMPAD7,
			KEY_NUMPAD8 = VK_NUMPAD8,
			KEY_NUMPAD9 = VK_NUMPAD9,

			KEY_ADD = VK_ADD,
			KEY_SUBTRACT = VK_SUBTRACT,
			KEY_MULTIPLY = VK_MULTIPLY,
			KEY_DIVIDE = VK_DIVIDE,
			KEY_SEPARATOR = VK_SEPARATOR,
			KEY_DECIMAL = VK_DECIMAL,
			KEY_PAUSE = VK_PAUSE
		};
#else
#endif
	}
}