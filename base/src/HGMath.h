#pragma once

// MathFu includes (https://github.com/google/mathfu)
#include <mathfu\utilities.h>
#include <mathfu\vector.h>
#include <mathfu\rect.h>
#include <mathfu\matrix.h>
#include <mathfu\quaternion.h>
#include <mathfu\constants.h>
#include <mathfu\io.h>

#define PI 3.141592653589793
#define DEGREES_TO_RADIANS(X) (X / 180.0 * PI)
#define RADIANS_TO_DEGREES(X) (X / PI * 180.0)

namespace hg
{
	namespace Math
	{
		static mathfu::Matrix<float, 4, 4> GetTranslationMatrix(const float& translationX, const float& translationY, const float& translationZ)
		{
			return mathfu::Matrix<float, 4, 4>(
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				translationX, translationY, translationZ, 1.0f);
		}

		static mathfu::Matrix<float, 4, 4> GetScaleMatrix(const float& scaleX, const float& scaleY, const float& scaleZ)
		{
			return mathfu::Matrix<float, 4, 4>(
				scaleX, 0.0f, 0.0f, 0.0f,
				0.0f, scaleY, 0.0f, 0.0f,
				0.0f, 0.0f, scaleZ, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f);
		}

		static mathfu::Matrix<float, 4, 4> GetRotationMatrix(const float& rotationX, const float& rotationY, const float& rotationZ) 
		{
			return mathfu::Matrix<float, 4, 4>(
				cosf(rotationZ) * cosf(rotationY), -sinf(rotationZ) * cosf(rotationX) + cosf(rotationZ) * sinf(rotationY) * sinf(rotationX), sinf(rotationZ) * sinf(rotationX) + cosf(rotationZ) * sinf(rotationY) * cosf(rotationX), 0.0f,
				sinf(rotationZ) * cosf(rotationY), cosf(rotationZ) * cosf(rotationX) + sinf(rotationZ) * sinf(rotationY) * sinf(rotationX), -cosf(rotationZ) * sinf(rotationX) + sinf(rotationZ) * sinf(rotationY) * cosf(rotationX), 0.0f,
				-sinf(rotationY), cosf(rotationY) * sinf(rotationX), cosf(rotationY) * cosf(rotationX), 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f);
		}

		static mathfu::Matrix<float, 4, 4> GetTransformationMatrix(
			const float& translationX, const float& translationY, const float& translationZ,
			const float& scaleX, const float& scaleY, const float& scaleZ,
			const float& rotationX, const float& rotationY, const float& rotationZ)
		{
			return GetTranslationMatrix(translationX, translationY, translationZ) * GetRotationMatrix(rotationX, rotationY, rotationZ) * GetScaleMatrix(scaleX, scaleY, scaleZ);
		}

		static mathfu::Matrix<float, 4, 4> GetViewMatrix(const mathfu::Vector<float, 3>& position, const mathfu::Vector<float, 3>& right, const mathfu::Vector<float, 3>& up, const mathfu::Vector<float, 3>& forward)
		{
			return mathfu::Matrix<float, 4, 4>(
				mathfu::Vector<float, 4>(right, 0.0f),
				mathfu::Vector<float, 4>(up, 0.0f),
				mathfu::Vector<float, 4>(forward, 0.0f),
				mathfu::Vector<float, 4>(-position, 1.0f));
		}

		// Reversed-Z projection matrix for better depth precision.
		static mathfu::Matrix<float, 4, 4> GetReversedZProjectionMatrix(const float& vFOV, const float& aspect, const float& zNear)
		{
			float scaleY = 1 / tan(vFOV / 2.0f);
			float scaleX = scaleY / aspect;

			return mathfu::Matrix<float, 4, 4>(
				scaleX, 0.0f, 0.0f, 0.0f,
				0.0f, scaleY, 0.0f, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f,
				0.0f, 0.0f, zNear, 0.0f);
		}

		static mathfu::Matrix<float, 4, 4> GetPerspectiveProjectionMatrix(const float& vFOV, const float& aspect, const float& zNear)
		{
			float scaleY = 1 / tan(vFOV / 2.0f);
			float scaleX = scaleY / aspect;

			return mathfu::Matrix<float, 4, 4>(
				scaleX, 0.0f, 0.0f, 0.0f,
				0.0f, scaleY, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 1.0f,
				0.0f, 0.0f, -zNear, 0.0f);
		}

		static mathfu::Matrix<float, 4, 4> GetOrthographicProjectionMatrix(const float& orthoHeight, const float& aspect, const float& zNear)
		{
			float scaleY = orthoHeight;
			float scaleX = scaleY / aspect;

			return mathfu::Matrix<float, 4, 4>(
				scaleX, 0.0f, 0.0f, 0.0f,
				0.0f, scaleY, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, -zNear, 1.0f);
		}
	}
}
