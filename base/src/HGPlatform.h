#pragma once

#include <assert.h>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <exception>
#include <string>

// TODO : Come up with a better error system.
#define HGFAILED(result)	(((HGRESULT)(result)) < 0)

enum HGRESULT
{
	HG_OK		= 0,
	HG_WARNING	= 1,
	HG_ERROR	= -1
};

inline void ThrowIfFailed(HGRESULT result)
{
	if (HGFAILED(result))
	{
		throw std::exception();
	}
}

/////////////////////////////////////////////////////////////////////////////////////////

#if defined(_WIN32)

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers.
#endif

#include <windows.h>
#include <wrl.h>

using namespace Microsoft::WRL;

inline void ThrowIfFailed(HRESULT hr)
{
	if (FAILED(hr))
	{
		throw std::exception();
	}
}

#endif