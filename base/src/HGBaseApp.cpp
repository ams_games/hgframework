#include "HGBaseApp.h"

hg::BaseApp::BaseApp(const UINT & width, const UINT & height, const std::wstring & name) :
	m_windowWidth(width),
	m_windowHeight(height),
	m_aspectRatio(((float)width) / ((float)height)),
	m_name(name),
	m_running(false),
	m_resizing(false),
	m_maximized(false),
	m_cursorVisible(true)
{
	// Initialize the different paths for the application and assets.
	WCHAR path[512];
	FileSystem::GetApplicationPath(path, _countof(path));
	m_applicationPath = path;
	FileSystem::GetAssetsPath(path, _countof(path));
	m_assetsPath = path;
	FileSystem::GetShadersPath(path, _countof(path));
	m_shadersPath = path;
	FileSystem::GetTexturesPath(path, _countof(path));
	m_texturesPath = path;
	FileSystem::GetModelsPath(path, _countof(path));
	m_modelsPath = path;
}

int hg::BaseApp::RunApp()
{
	MSG msg = {};
	if (this->initPlatform())
	{
		if (!this->start()) this->CloseApp();
		m_running = true;

		// Record the initial timestamp.
		QueryPerformanceFrequency(&m_timestampFrequency);
		QueryPerformanceCounter(&m_startTimestamp);
		while (m_running)
		{
			// Process all messages in the queue.
			while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			// Update this frame's timestamp.
			QueryPerformanceCounter(&m_currentFrameTimestamp);

			// Update the application.
			if (!this->update()) this->CloseApp();

			// Render the application.
			if (!this->render()) this->CloseApp();

			// Record this frame's timestamp.
			m_lastFrameTimestamp = m_currentFrameTimestamp;
		}
		this->end();
	}
	this->destroyPlatform();

	// Return this part of the WM_QUIT message to Windows.
	return static_cast<char>(msg.wParam);
}

void hg::BaseApp::CloseApp()
{
	m_running = false;
}

void hg::BaseApp::UpdateWindowSize(const UINT & width, const UINT & height)
{
	m_windowWidth = width;
	m_windowHeight = height;
	m_aspectRatio = (static_cast<float>(m_windowWidth) / static_cast<float>(m_windowHeight));
}

void hg::BaseApp::ParseCmdLineArgs(const char * cmds)
{
	// TODO : Add specific application debugging options / modes.
}

std::wstring hg::BaseApp::GetAssetFullPath(LPCWSTR assetName, FileSystem::AssetType assetType)
{
	std::wstring path;
	switch (assetType)
	{
	default:
	case FileSystem::DEFAULT:
		path = m_assetsPath + assetName;
		break;
	case FileSystem::SHADER:
		path = m_shadersPath + assetName;
		break;
	case FileSystem::TEXTURE:
		path = m_texturesPath + assetName;
		break;
	case FileSystem::MODEL:
		path = m_modelsPath + assetName;
		break;
	}

	return path;
}
