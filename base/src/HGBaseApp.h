#pragma once

#include "HGPlatform.h"
#include "HGContainers.h"
#include "HGMath.h"
#include "HGFileSystem.h"
#include "HGInput.h"
#include "HGGraphics.h"

namespace hg
{
	class BaseApp {
	public:
		BaseApp(const UINT & width,const UINT & height, const std::wstring &name);

		int RunApp();						// Main application loop
		void CloseApp();					// Exit function

		// Window info
		std::wstring GetName() { return m_name; };
		UINT GetWindowWidth() { return m_windowWidth; }
		UINT GetWindowHeight() { return m_windowHeight; }

		// Time functions
		double GetApplicationTime() { return static_cast<double>(m_currentFrameTimestamp.QuadPart - m_startTimestamp.QuadPart) / static_cast<double>(m_timestampFrequency.QuadPart); }
		double GetFrameDelta() { return static_cast<double>(m_currentFrameTimestamp.QuadPart - m_lastFrameTimestamp.QuadPart) / static_cast<double>(m_timestampFrequency.QuadPart); }

		// Window resizing.
		virtual void SetResizing(bool resizing) { m_resizing = resizing; }
		virtual bool IsResizing() { return m_resizing; }
		virtual void SetMaximized(bool maximized) { m_maximized = maximized; }
		virtual bool IsMaximized() { return m_maximized; }
		virtual void UpdateWindowSize(const UINT & width, const UINT & height);
		virtual void ResizeWindow() {};

		// Command line args parsing.
		virtual void ParseCmdLineArgs(const char * cmds);

		// Input methods.
		virtual bool IsKeyPressed(const UINT8 &key) = 0;
		virtual void OnKeyUp(const UINT8 &key) {};
		virtual void OnMouseMove(const UINT &xCoord, const UINT &yCoord) {};
		virtual void OnMouseButton(const UINT &xCoord, const UINT &yCoord, const Input::MouseButton &mouseButton, const bool &buttonDown) {};
		virtual void OnMouseWheel(const UINT &xCoord, const UINT &yCoord, const float &wheelDelta) {};
		// TODO : Add joystick input methods.

		// Cursor handling.
		bool IsCursorVisible() { return m_cursorVisible; }
		virtual void GetWindowCenter(UINT& centerX, UINT& centerY) = 0;
		virtual void TrapCursor() = 0;
		virtual void FreeCursor() = 0;

	protected:
		std::wstring GetAssetFullPath(LPCWSTR assetName, FileSystem::AssetType assetType);

		bool m_running;
		std::wstring m_name;

		bool m_resizing;
		bool m_maximized;
		UINT m_windowWidth;
		UINT m_windowHeight;
		float m_aspectRatio;

		bool m_cursorVisible;

		Graphics::Renderer * m_renderer;

	private:
		// Platform initialization/destruction.
		virtual bool initPlatform() = 0;
		virtual bool destroyPlatform() = 0;

		// Main application methods.
		virtual bool start() = 0;			// Resource allocation.
		virtual bool update() = 0;			// Logic update.
		virtual bool render() = 0;			// Main Render loop.
		virtual bool end() = 0;				// Free allocated resources.

		// Application and asset paths
		std::wstring m_applicationPath;
		std::wstring m_assetsPath;
		std::wstring m_shadersPath;
		std::wstring m_texturesPath;
		std::wstring m_modelsPath;

		// Time variables
		LARGE_INTEGER m_startTimestamp;
		LARGE_INTEGER m_lastFrameTimestamp;
		LARGE_INTEGER m_currentFrameTimestamp;
		LARGE_INTEGER m_timestampFrequency;
	};
}