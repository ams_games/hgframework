#pragma once

#include "HGGraphics.h"

namespace hg
{
	namespace Graphics
	{
		class Buffer 
		{
		public:
			UINT size;
		};

		class Texture {};

		class RenderTarget
		{
		public:
			UINT width;
			UINT height;
			float clearColor[4];
		};

		class Sampler {};

		class RenderSubpass {};

		class RenderPass {};
	}
}