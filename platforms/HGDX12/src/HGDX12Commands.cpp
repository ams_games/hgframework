#include "HGDX12Commands.h"

#include "HGDX12Resources.h"
#include "HGDX12Pipeline.h"

///////////// COMMMAND RECORDER /////////////

void hg::Graphics::DX12CommandRecorder::Reset()
{
	ThrowIfFailed(cmdList->Reset(cmdAllocator, NULL));
}

void hg::Graphics::DX12CommandRecorder::Close()
{
	ThrowIfFailed(cmdList->Close());
}

void hg::Graphics::DX12CommandRecorder::ExecuteCommandBundles(unsigned int nCmdBundles, CommandBundle *const *ppCommandBundles)
{
	for (UINT i = 0; i < nCmdBundles; i++)
		cmdList->ExecuteBundle(static_cast<const DX12CommandBundle*>(ppCommandBundles[i])->cmdList);
}

void hg::Graphics::DX12CommandRecorder::BeginRenderPass(const RenderPass * pRenderPass)
{
	const DX12RenderPass* pDX12RenderPass = static_cast<const DX12RenderPass*>(pRenderPass);

	D3D12_RESOURCE_BARRIER beginRenderPassBarrier = {};
	beginRenderPassBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	beginRenderPassBarrier.Transition.pResource = pDX12RenderPass->framebuffers[0];
	beginRenderPassBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
	beginRenderPassBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
	beginRenderPassBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;

	cmdList->ResourceBarrier(1, &beginRenderPassBarrier);
	cmdList->OMSetRenderTargets(1, &pDX12RenderPass->framebuffersCPUHandles[0], FALSE, nullptr);
	cmdList->ClearRenderTargetView(pDX12RenderPass->framebuffersCPUHandles[0], pDX12RenderPass->clearColor, 0, nullptr);
}

void hg::Graphics::DX12CommandRecorder::BeginMainRenderPass(const RenderPass * pRenderPass, const UINT currentFramebufferIdx)
{
	const DX12RenderPass* pDX12RenderPass = static_cast<const DX12RenderPass*>(pRenderPass);

	D3D12_RESOURCE_BARRIER beginRenderPassBarrier = {};
	beginRenderPassBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	beginRenderPassBarrier.Transition.pResource = pDX12RenderPass->framebuffers[currentFramebufferIdx];
	beginRenderPassBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
	beginRenderPassBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
	beginRenderPassBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;

	cmdList->ResourceBarrier(1, &beginRenderPassBarrier);
	cmdList->OMSetRenderTargets(1, &pDX12RenderPass->framebuffersCPUHandles[currentFramebufferIdx], FALSE, nullptr);
	cmdList->ClearRenderTargetView(pDX12RenderPass->framebuffersCPUHandles[currentFramebufferIdx], pDX12RenderPass->clearColor, 0, nullptr);
}

void hg::Graphics::DX12CommandRecorder::AdvanceSubpass()
{
	assert(0);
}

void hg::Graphics::DX12CommandRecorder::EndRenderPass()
{
	D3D12_RESOURCE_BARRIER endRenderPassBarrier = {};
	endRenderPassBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	endRenderPassBarrier.Transition.pResource = static_cast<const DX12RenderPass*>(pRenderPass)->framebuffers[currentFramebufferIdx];
	endRenderPassBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
	endRenderPassBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
	endRenderPassBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;

	cmdList->ResourceBarrier(1, &endRenderPassBarrier);
}

void hg::Graphics::DX12CommandRecorder::SetPipelineLayout(const PipelineLayout * pPipelineLayout)
{
	cmdList->SetGraphicsRootSignature((static_cast<const DX12PipelineLayout *>(pPipelineLayout))->handle);
}

void hg::Graphics::DX12CommandRecorder::SetPipelineState(const PipelineState * pPipelineState)
{
	cmdList->SetPipelineState((static_cast<const DX12PipelineState *>(pPipelineState))->handle);
}

void hg::Graphics::DX12CommandRecorder::SetPrimitiveTopology(PrimitiveTopology primitiveTopology)
{
	cmdList->IASetPrimitiveTopology(static_cast<D3D12_PRIMITIVE_TOPOLOGY>(primitiveTopology));
}

void hg::Graphics::DX12CommandRecorder::SetVertexBuffers(UINT start, UINT nViews, Buffer * const * ppBuffers)
{
	tinystl::vector<D3D12_VERTEX_BUFFER_VIEW> vertexbufferArray(nViews);

	for (UINT i = 0; i < nViews; i++)
		vertexbufferArray[i] = static_cast<const DX12Buffer*>(ppBuffers[i])->vbv;

	cmdList->IASetVertexBuffers(start, nViews, vertexbufferArray.data());
}

void hg::Graphics::DX12CommandRecorder::SetIndexBuffer(Buffer * pBuffer)
{
	cmdList->IASetIndexBuffer(&static_cast<DX12Buffer*>(pBuffer)->ibv);
}

void hg::Graphics::DX12CommandRecorder::SetScissorRects(UINT nRects, const Rect * pRects)
{
	cmdList->RSSetScissorRects(nRects, static_cast<const D3D12_RECT *>(pRects));
}

void hg::Graphics::DX12CommandRecorder::SetViewports(UINT nViewports, const Viewport * pViewports)
{

	cmdList->RSSetViewports(nViewports, static_cast<const D3D12_VIEWPORT *>(pViewports));
}

void hg::Graphics::DX12CommandRecorder::SetBlendFactor(const FLOAT blendFactors[4])
{
	cmdList->OMSetBlendFactor(blendFactors);
}

void hg::Graphics::DX12CommandRecorder::SetStencilRefValue(UINT stencilRefValue)
{
	cmdList->OMSetStencilRef(stencilRefValue);
}

void hg::Graphics::DX12CommandRecorder::Draw(const UINT & nVertices, const UINT & nInstances, const UINT & vertexStart, const UINT & instanceStart)
{
	cmdList->DrawInstanced(nVertices, nInstances, vertexStart, instanceStart);
}

void hg::Graphics::DX12CommandRecorder::DrawIndexed(const UINT & nIndices, const UINT & nInstances, const UINT & indexStart, const UINT & vertexOffset, const UINT & instanceStart)
{
	cmdList->DrawIndexedInstanced(nIndices, nInstances, indexStart, vertexOffset, instanceStart);
}

///////////// COMMMAND BUNDLE /////////////

void hg::Graphics::DX12CommandBundle::Reset()
{
	ThrowIfFailed(cmdList->Reset(cmdAllocator, NULL));
}

void hg::Graphics::DX12CommandBundle::Close()
{
	ThrowIfFailed(cmdList->Close());
}

void hg::Graphics::DX12CommandBundle::SetPipelineLayout(const PipelineLayout * pPipelineLayout)
{
	cmdList->SetGraphicsRootSignature((static_cast<const DX12PipelineLayout *>(pPipelineLayout))->handle);
}

void hg::Graphics::DX12CommandBundle::SetPipelineState(const PipelineState * pPipelineState)
{
	cmdList->SetPipelineState((static_cast<const DX12PipelineState *>(pPipelineState))->handle);
}

void hg::Graphics::DX12CommandBundle::SetPrimitiveTopology(PrimitiveTopology primitiveTopology)
{
	cmdList->IASetPrimitiveTopology(static_cast<D3D12_PRIMITIVE_TOPOLOGY>(primitiveTopology));
}

void hg::Graphics::DX12CommandBundle::SetVertexBuffers(UINT start, UINT nViews, Buffer * const * ppBuffers)
{
	tinystl::vector<D3D12_VERTEX_BUFFER_VIEW> vertexbufferArray(nViews);

	for (UINT i = 0; i < nViews; i++)
		vertexbufferArray[i] = static_cast<const DX12Buffer*>(ppBuffers[i])->vbv;

	cmdList->IASetVertexBuffers(start, nViews, vertexbufferArray.data());
}

void hg::Graphics::DX12CommandBundle::SetBlendFactor(const FLOAT blendFactors[4])
{
	cmdList->OMSetBlendFactor(blendFactors);
}

void hg::Graphics::DX12CommandBundle::SetStencilRefValue(UINT stencilRefValue)
{
	cmdList->OMSetStencilRef(stencilRefValue);
}

void hg::Graphics::DX12CommandBundle::DrawInstanced(UINT verticesPerInstance, UINT nInstances, UINT vertexStart, UINT instanceStart)
{
	cmdList->DrawInstanced(verticesPerInstance, nInstances, vertexStart, instanceStart);
}
