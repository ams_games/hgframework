#pragma once

#include "base\src\HGGraphics.h"

#include <d3d12.h>
#include <dxgi1_6.h>
#include <d3dcompiler.h>
#include "pix3.h"

#include "HGDX12Shader.h"
#include "HGDX12Resources.h"
#include "HGDX12Pipeline.h"
#include "HGDX12Commands.h"

namespace hg
{
	namespace Graphics
	{
		// ENUMS

		enum TextureFormat
		{
			DEFAULT = DXGI_FORMAT_UNKNOWN,
			R8G8B8A8_UNORM = DXGI_FORMAT_R8G8B8A8_UNORM,
			R8G8B8A8_SRGB = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB,
			D32 = DXGI_FORMAT_D32_FLOAT
		};

		// STRUCTS

		struct Rect : public D3D12_RECT
		{
			Rect() {};
			Rect(const LONG& l, const LONG& t, const LONG& r, const LONG& b)
			{
				this->left = l;
				this->top = t;
				this->right = r;
				this->bottom = b;
			}
		};
		struct Viewport : public D3D12_VIEWPORT
		{
			Viewport() {};
			Viewport(const float& originX, const float& originY, const float& width, const float& height, const float& minDepth, const float& maxDepth)
			{
				this->TopLeftX = originX;
				this->TopLeftY = originY;
				this->Width = width;
				this->Height = height;
				this->MinDepth = minDepth;
				this->MaxDepth = maxDepth;
			}
		};

		// INTERFACES

		class DX12Shader;
		class DX12Buffer;
		class DX12Texture;
		class DX12RenderTarget;
		class DX12RenderSubpass;
		class DX12RenderPass;
		class DX12PipelineLayout;
		class DX12PipelineState;
		class DX12CommandRecorder;
		class DX12CommandBundle;
		class DX12Renderer;
	}
}