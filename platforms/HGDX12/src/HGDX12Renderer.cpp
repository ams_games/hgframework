#include "HGDX12Renderer.h"

#include "HGWinApp.h"
#include <fstream>

extern hg::BaseApp* application;

hg::Graphics::DX12Renderer::DX12Renderer()
{
	// Enable D3D12 debug layer
#if defined(_DEBUG)
	{
		ComPtr<ID3D12Debug> debugController;
		if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
		{
			debugController->EnableDebugLayer();
		}
	}
#endif

	// Create the DXGI factory
	ComPtr<IDXGIFactory4> factory;
	ThrowIfFailed(CreateDXGIFactory1(IID_PPV_ARGS(&factory)));

	// Find the first hardware adapter available
	ComPtr<IDXGIAdapter1> hardwareAdapter;
	{
		for (UINT adapterIndex = 0; DXGI_ERROR_NOT_FOUND != factory->EnumAdapters1(adapterIndex, &hardwareAdapter); ++adapterIndex)
		{
			DXGI_ADAPTER_DESC1 desc;
			hardwareAdapter->GetDesc1(&desc);

			if (desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
			{
				// Don't select the Basic Render Driver adapter.
				// If you want a software adapter, pass in "/warp" on the command line.
				continue;
			}

			// Check to see if the adapter supports Direct3D 12, but don't create the
			// actual device yet.
			if (SUCCEEDED(D3D12CreateDevice(hardwareAdapter.Get(), D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), nullptr)))
			{
				break;
			}
		}
	}

	// Create the device
	ThrowIfFailed(D3D12CreateDevice(
		hardwareAdapter.Get(),
		D3D_FEATURE_LEVEL_11_0,
		IID_PPV_ARGS(&m_device)
	));

	// TODO : Add a compute and a copy queue, and have 3 different rendering threads for all of them
	// Create the command queue
	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

	ThrowIfFailed(m_device->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_mainQueue)));

	// Create the swapchain
	DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
	swapChainDesc.BufferCount = 2;
	swapChainDesc.Width = application->GetWindowWidth();
	swapChainDesc.Height = application->GetWindowHeight();
	swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.SampleDesc.Count = 1;

	ComPtr<IDXGISwapChain1> swapchain;
	ThrowIfFailed(factory->CreateSwapChainForHwnd(
		m_mainQueue,		// Swap chain needs the queue so that it can force a flush on it.
		((WinApp*)application)->GetWindow(),
		&swapChainDesc,
		nullptr,
		nullptr,
		&swapchain
	));

	// TODO : Add fullscreen support
	ThrowIfFailed(factory->MakeWindowAssociation(((WinApp*)application)->GetWindow(), DXGI_MWA_NO_ALT_ENTER));

	ThrowIfFailed(swapchain.As(&m_swapchain));
	m_mainFramebufferIdx = m_swapchain->GetCurrentBackBufferIndex();

	// Create descriptor heaps.
	{
		// Describe and create a render target view (RTV) descriptor heap.
		D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
		rtvHeapDesc.NumDescriptors = 2;
		rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		ThrowIfFailed(m_device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&m_descHeap)));
	}

	// Create the command allocators.
	ThrowIfFailed(m_device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&m_mainCmdAllocator)));
	ThrowIfFailed(m_device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_BUNDLE, IID_PPV_ARGS(&m_bundleCmdAllocator)));

	// Create synchronization objects.
	{
		ThrowIfFailed(m_device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_fence)));
		m_fenceValue = 1;

		// Create an event handle to use for frame synchronization.
		m_fenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
		if (m_fenceEvent == nullptr)
		{
			ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
		}
	}
}

hg::Graphics::DX12Renderer::~DX12Renderer()
{
	// TODO : Release all resources.

}

hg::Graphics::Shader * hg::Graphics::DX12Renderer::LoadShader(const LPWSTR & vertexShaderName, const LPWSTR & tessControlShaderName, const LPWSTR & tessEvaluationShaderName, const LPWSTR & geometryShaderName, const LPWSTR & fragmentShaderName, const LPWSTR & computeShaderName)
{
	DX12Shader* shader = new DX12Shader();

	// Look for the compiled shaders path
	WCHAR compiledShadersPath[512];
	DWORD size = GetModuleFileName(nullptr, compiledShadersPath, 512);
	if (size == 0 || size == 512)
	{
		// Method failed or path was truncated.
		throw std::exception();
	}
	WCHAR* lastSlash = wcsrchr(compiledShadersPath, L'\\');
	if (lastSlash)
	{
		wcscpy_s(lastSlash, sizeof(L"\\shaders\\") / _WCHAR_T_SIZE, L"\\shaders\\");
	}

	// Create all the shader modules from the compiled files.
	std::wstring inputFiles[SHADER_STAGES_COUNT] = {
		std::wstring(vertexShaderName) + L".vert.spv" ,
		std::wstring(tessControlShaderName) + L".tesc.spv" ,
		std::wstring(tessEvaluationShaderName) + L".tese.spv" ,
		std::wstring(geometryShaderName) + L".geom.spv" ,
		std::wstring(fragmentShaderName) + L".frag.spv" ,
		std::wstring(computeShaderName) + L".comp.spv" };
	ShaderData* outputShaders[SHADER_STAGES_COUNT] = { &shader->vertexStage, &shader->tesselationControlStage,
		&shader->tesselationEvaluationStage, &shader->geometryStage, &shader->fragmentStage,&shader->computeStage };

	for (UINT i = 0; i < SHADER_STAGES_COUNT; i++)
	{
		std::ifstream file(compiledShadersPath + inputFiles[i], std::ifstream::binary);
		if (file.is_open())
		{
			std::wstring shaderPath = compiledShadersPath + inputFiles[i];
			D3DReadFileToBlob(shaderPath.c_str(), &outputShaders[i]->blob);
		}
	}

	return shader;
}

void hg::Graphics::DX12Renderer::DestroyShader(Shader * pShader)
{
	DX12Shader* pDX12Shader = static_cast<DX12Shader*>(pShader);

	pDX12Shader->vertexStage.blob->Release();
	pDX12Shader->vertexStage.blob->Release();

	delete pShader;
	pShader = nullptr;
}

hg::Graphics::Texture * hg::Graphics::DX12Renderer::LoadTexture(const LPWSTR & textureName)
{
	assert(0);
}

void hg::Graphics::DX12Renderer::DestroyTexture(Texture * pTexture)
{
	assert(0);
}

hg::Graphics::Buffer * hg::Graphics::DX12Renderer::CreateBuffer(const UINT & bufferSize, const UINT & elementStride, const UINT & bufferUsageFlags, const void * pData)
{
	DX12Buffer * buffer = new DX12Buffer();

	// Note: using upload heaps to transfer static data like vert buffers is not 
	// recommended. Every time the GPU needs it, the upload heap will be marshalled 
	// over. Please read up on Default Heap usage. An upload heap is used here for 
	// code simplicity and because there are very few verts to actually transfer.
	D3D12_HEAP_PROPERTIES heapProp;
	heapProp.Type = D3D12_HEAP_TYPE_UPLOAD;
	heapProp.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	heapProp.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	heapProp.CreationNodeMask = 1;
	heapProp.VisibleNodeMask = 1;

	D3D12_RESOURCE_DESC vbDesc;
	vbDesc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
	vbDesc.Alignment = 0;
	vbDesc.Width = bufferSize;
	vbDesc.Height = 1;
	vbDesc.DepthOrArraySize = 1;
	vbDesc.MipLevels = 1;
	vbDesc.Format = DXGI_FORMAT_UNKNOWN;
	vbDesc.SampleDesc.Count = 1;
	vbDesc.SampleDesc.Quality = 0;
	vbDesc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
	vbDesc.Flags = D3D12_RESOURCE_FLAG_NONE;

	ThrowIfFailed(m_device->CreateCommittedResource(
		&heapProp,
		D3D12_HEAP_FLAG_NONE,
		&vbDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&buffer->handle)));

	// Fill the data with data if providen.
	if (pData)
	{
		UINT8* pMappedData;
		D3D12_RANGE readRange = { 0, 0 };
		ThrowIfFailed(buffer->handle->Map(0, &readRange, reinterpret_cast<void**>(&pMappedData)));
		memcpy(pMappedData, pData, bufferSize);
		buffer->handle->Unmap(0, nullptr);
	}

	// Initialize the vertex buffer view.
	buffer->vbv.BufferLocation = buffer->handle->GetGPUVirtualAddress();
	buffer->vbv.StrideInBytes = elementStride;
	buffer->vbv.SizeInBytes = bufferSize;

	return buffer;
}

void hg::Graphics::DX12Renderer::UpdateBuffer(Buffer const * pBuffer, void const * pData)
{
	assert(0);
}

void hg::Graphics::DX12Renderer::DestroyBuffer(Buffer * pBuffer)
{
	DX12Buffer* pDX12Buffer = static_cast<DX12Buffer*>(pBuffer);

	pDX12Buffer->handle->Release();

	delete pBuffer;
	pBuffer = nullptr;
}

hg::Graphics::RenderTarget * hg::Graphics::DX12Renderer::CreateRenderTarget(const UINT & renderPassIdx, const UINT & width, const UINT & height, const UINT & format, const float clearColor[4], const SampleCount & samples, const TextureDimensions & dimensions, const UINT & layers, const UINT & mips, const bool & sampled)
{
	assert(0);
}

void hg::Graphics::DX12Renderer::DestroyRenderTarget(RenderTarget * pRenderTarget)
{
	assert(0);
}

hg::Graphics::Sampler * hg::Graphics::DX12Renderer::CreateSampler(const FilteringMode & minFiltering, const FilteringMode & magFiltering, const FilteringMode & mipFiltering, const TextureAddressMode & modeU, const TextureAddressMode & modeV, const TextureAddressMode & modeW, const UINT & maxAnisotropy, const TextureBorderColor & borderColor, const float & mipLODBias, const float & minLOD, const float & maxLOD, const CompareOp & compareOp)
{
	assert(0);
}

void hg::Graphics::DX12Renderer::DestroySampler(Sampler * pSampler)
{
	assert(0);
}

hg::Graphics::RenderSubpass * hg::Graphics::DX12Renderer::CreateRenderSubpass(const PipelineType & type, const UINT & subpassIdx, const UINT & nInputRTs, RenderTarget * const * ppInputRTs, const UINT & nOutputRTs, RenderTarget * const * ppOutputRTs, RenderTarget * const * ppResolveRTs, const bool & depthWrite, RenderTarget const * pDepthBuffer, const UINT & nPreservedRTs, RenderTarget * const * ppPreservedRTs, const UINT & nDependencies, UINT const * ppDependencies)
{
	assert(0);
}

void hg::Graphics::DX12Renderer::DestroyRenderSubpass(RenderSubpass * pRenderSubpass)
{
	assert(0);
}

hg::Graphics::RenderPass * hg::Graphics::DX12Renderer::GetMainRenderPass()
{
	assert(0);
}

hg::Graphics::RenderPass * hg::Graphics::DX12Renderer::CreateRenderPass(const UINT & width, const UINT & height, const float framebufferClearColor[4], const UINT & nSubpasses, RenderSubpass * const * ppSubpasses)
{
	assert(0);
}

/*hg::Graphics::RenderPass * hg::Graphics::DX12Renderer::CreateMainRenderPass(const float* clearColor)
{
	DX12RenderPass * mainRenderPass = new DX12RenderPass();
	if (clearColor)
	{
		for (UINT i = 0; i < 4; i++)
		{
			mainRenderPass->clearColor[i] = clearColor[i];
		}
	}

	mainRenderPass->framebuffers.resize(2);
	mainRenderPass->framebuffersCPUHandles.resize(2);

	// Create frame resources.
	{
		D3D12_CPU_DESCRIPTOR_HANDLE rtvDesc = m_descHeap->GetCPUDescriptorHandleForHeapStart();
		UINT rtvDescSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

		// Create a RTV for each frame.
		for (UINT n = 0; n < 2; n++)
		{
			mainRenderPass->framebuffersCPUHandles[n] = rtvDesc;
			ThrowIfFailed(m_swapchain->GetBuffer(n, IID_PPV_ARGS(&mainRenderPass->framebuffers[n])));
			m_device->CreateRenderTargetView(mainRenderPass->framebuffers[n], nullptr, rtvDesc);
			rtvDesc.ptr += rtvDescSize;
		}
	}

	return mainRenderPass;
}*/

void hg::Graphics::DX12Renderer::DestroyRenderPass(RenderPass * pRenderPass)
{
	DX12RenderPass* pDX12RenderPass = static_cast<DX12RenderPass*>(pRenderPass);

	for (int i = 0; i < 2; i++)
		pDX12RenderPass->framebuffers[i]->Release();

	delete pRenderPass;
	pRenderPass = nullptr;
}

hg::Graphics::PipelineLayout * hg::Graphics::DX12Renderer::CreatePipelineLayout(const Shader * pShader)
{
	assert(0);

	// Create empty root signature for now.
	DX12PipelineLayout * pipelineLayout = new DX12PipelineLayout();

	D3D12_ROOT_SIGNATURE_DESC pipelineLayoutDesc = { 0 };
	pipelineLayoutDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;

	ComPtr<ID3DBlob> signature;
	ComPtr<ID3DBlob> error;
	ThrowIfFailed(D3D12SerializeRootSignature(&pipelineLayoutDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error));
	ThrowIfFailed(m_device->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&pipelineLayout->handle)));

	return pipelineLayout;
}

void hg::Graphics::DX12Renderer::DestroyPipelineLayout(PipelineLayout * pPipelineLayout)
{
	DX12PipelineLayout* pDX12PipelineLayout = static_cast<DX12PipelineLayout*>(pPipelineLayout);

	pDX12PipelineLayout->handle->Release();

	delete pPipelineLayout;
	pPipelineLayout = nullptr;
}

hg::Graphics::PipelineState * hg::Graphics::DX12Renderer::CreatePipelineState(const Shader * pShader, const PipelineLayout * pPipelineLayout, const RenderPass * pRenderPass, const UINT & subpassIdx, const CullMode & cullMode)
{
	assert(0);

	DX12PipelineState* pipelineState = new DX12PipelineState();

	// Define the vertex input layout.
	D3D12_INPUT_ELEMENT_DESC inputElementDescs[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};

	// Describe and create the graphics pipeline state object.
	D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc = {};
	pipelineStateDesc.InputLayout = { inputElementDescs, _countof(inputElementDescs) };
	pipelineStateDesc.pRootSignature = static_cast<const DX12PipelineLayout*>(pPipelineLayout)->handle;

	const DX12Shader* pDX12Shader = static_cast<const DX12Shader*>(pShader);
	D3D12_SHADER_BYTECODE VSBytecode, PSBytecode;
	VSBytecode.pShaderBytecode = pDX12Shader->vertexStage.blob->GetBufferPointer();
	VSBytecode.BytecodeLength = pDX12Shader->vertexStage.blob->GetBufferSize();
	PSBytecode.pShaderBytecode = pDX12Shader->fragmentStage.blob->GetBufferPointer();
	PSBytecode.BytecodeLength = pDX12Shader->fragmentStage.blob->GetBufferSize();
	pipelineStateDesc.VS = VSBytecode;
	pipelineStateDesc.PS = PSBytecode;

	D3D12_RASTERIZER_DESC RSDesc;
	RSDesc.FillMode = D3D12_FILL_MODE_SOLID;
	RSDesc.CullMode = D3D12_CULL_MODE_BACK;
	RSDesc.FrontCounterClockwise = FALSE;
	RSDesc.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
	RSDesc.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
	RSDesc.SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
	RSDesc.DepthClipEnable = TRUE;
	RSDesc.MultisampleEnable = FALSE;
	RSDesc.AntialiasedLineEnable = FALSE;
	RSDesc.ForcedSampleCount = 0;
	RSDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
	pipelineStateDesc.RasterizerState = RSDesc;

	D3D12_BLEND_DESC BlendDesc;
	BlendDesc.AlphaToCoverageEnable = FALSE;
	BlendDesc.IndependentBlendEnable = FALSE;
	const D3D12_RENDER_TARGET_BLEND_DESC defaultRenderTargetBlendDesc =
	{
		FALSE,FALSE,
		D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
		D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
		D3D12_LOGIC_OP_NOOP,
		D3D12_COLOR_WRITE_ENABLE_ALL,
	};
	for (UINT i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i)
		BlendDesc.RenderTarget[i] = defaultRenderTargetBlendDesc;
	pipelineStateDesc.BlendState = BlendDesc;

	pipelineStateDesc.DepthStencilState.DepthEnable = FALSE;
	pipelineStateDesc.DepthStencilState.StencilEnable = FALSE;
	pipelineStateDesc.SampleMask = UINT_MAX;
	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	pipelineStateDesc.SampleDesc.Count = 1;
	ThrowIfFailed(m_device->CreateGraphicsPipelineState(&pipelineStateDesc, IID_PPV_ARGS(&pipelineState->handle)));

	return pipelineState;
}

void hg::Graphics::DX12Renderer::DestroyPipelineState(PipelineState * pPipelineState)
{
	DX12PipelineState* pDX12PipelineState = static_cast<DX12PipelineState*>(pPipelineState);

	pDX12PipelineState->handle->Release();

	delete pPipelineState;
	pPipelineState = nullptr;
}

hg::Graphics::CommandRecorder * hg::Graphics::DX12Renderer::CreateCommandRecorder(const CommandRecorderType & type)
{
	DX12CommandRecorder * commandRecorder = new DX12CommandRecorder();

	// Create the command list.
	ThrowIfFailed(m_device->CreateCommandList(0, static_cast<D3D12_COMMAND_LIST_TYPE>(type), m_mainCmdAllocator, nullptr, IID_PPV_ARGS(&commandRecorder->cmdList)));

	// Add a reference to its parent command allocator.
	commandRecorder->cmdAllocator = m_mainCmdAllocator;

	// Command lists are created in the recording state, but there is nothing
	// to record yet. The main loop expects it to be closed, so close it now.
	ThrowIfFailed(commandRecorder->cmdList->Close());

	return commandRecorder;
}

void hg::Graphics::DX12Renderer::DestroyCommandRecorder(CommandRecorder * pCommandRecorder)
{
	DX12CommandRecorder* pDX12CommandRecorder = static_cast<DX12CommandRecorder*>(pCommandRecorder);

	pDX12CommandRecorder->cmdList->Release();

	delete pCommandRecorder;
	pCommandRecorder = nullptr;
}

hg::Graphics::CommandBundle * hg::Graphics::DX12Renderer::CreateCommandBundle(const RenderPass * pRenderPass, const UINT & subpassIdx)
{
	DX12CommandBundle * commandBundle = new DX12CommandBundle();

	// Create the command bundle.
	ThrowIfFailed(m_device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_BUNDLE, m_bundleCmdAllocator, nullptr, IID_PPV_ARGS(&commandBundle->cmdList)));

	// Add a reference to its parent command allocator.
	commandBundle->cmdAllocator = m_bundleCmdAllocator;

	return commandBundle;
}

void hg::Graphics::DX12Renderer::DestroyCommandBundle(CommandBundle * pCommandBundle)
{
	DX12CommandBundle* pDX12CommandBundle = static_cast<DX12CommandBundle*>(pCommandBundle);

	pDX12CommandBundle->cmdList->Release();

	delete pCommandBundle;
	pCommandBundle = nullptr;
}

void hg::Graphics::DX12Renderer::BindTexture(const tinystl::string & name, Texture * pTexture, Sampler * pSampler, PipelineLayout * pPipelineLayout)
{
	assert(0);
}

void hg::Graphics::DX12Renderer::BindTexture(const tinystl::string & name, RenderTarget * pTexture, Sampler * pSampler, PipelineLayout * pPipelineLayout)
{
	assert(0);
}

void hg::Graphics::DX12Renderer::BindSampler(const tinystl::string & name, Sampler * pSampler, PipelineLayout * pPipelineLayout)
{
	assert(0);
}

void hg::Graphics::DX12Renderer::BindBuffer(const tinystl::string & name, Buffer * pBuffer, PipelineLayout * pPipelineLayout)
{
	assert(0);
}

void hg::Graphics::DX12Renderer::ExecuteCommands(unsigned int nCmdRecorders, CommandRecorder * const * ppCommandRecorders)
{
	tinystl::vector<ID3D12CommandList*> cmdListArray(nCmdRecorders);

	for (UINT i = 0; i < nCmdRecorders; i++)
		cmdListArray[i] = static_cast<DX12CommandRecorder*>(ppCommandRecorders[i])->cmdList;

	m_mainQueue->ExecuteCommandLists(nCmdRecorders, cmdListArray.data());
}

void hg::Graphics::DX12Renderer::FinishFrame()
{
	// Signal and increment the fence value.
	const UINT64 fence = m_fenceValue;
	ThrowIfFailed(m_mainQueue->Signal(m_fence, fence));
	m_fenceValue++;

	// Wait until the previous frame is finished.
	if (m_fence->GetCompletedValue() < fence)
	{
		ThrowIfFailed(m_fence->SetEventOnCompletion(fence, m_fenceEvent));
		WaitForSingleObject(m_fenceEvent, INFINITE);
	}

	m_mainFramebufferIdx = m_swapchain->GetCurrentBackBufferIndex();

	// Command list allocators can only be reset when the associated 
	// command lists have finished execution on the GPU; apps should use 
	// fences to determine GPU execution progress.
	ThrowIfFailed(m_mainCmdAllocator->Reset());
}

void hg::Graphics::DX12Renderer::Present()
{
	ThrowIfFailed(m_swapchain->Present(1, 0));
}

void hg::Graphics::DX12Renderer::ResizeMainFramebuffer()
{
	assert(0);

	DX12RenderPass* pDX12RenderPass = static_cast<DX12RenderPass*>(nullptr);

	// Release the previous buffers.
	for (UINT n = 0; n < 2; n++)
	{
		pDX12RenderPass->framebuffers[n]->Release();
	}

	// Resize the swapcahin buffers.
	m_swapchain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	// Recreate the render targets.
	{
		D3D12_CPU_DESCRIPTOR_HANDLE rtvDesc = m_descHeap->GetCPUDescriptorHandleForHeapStart();
		UINT rtvDescSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

		// Create a RTV for each frame.
		for (UINT n = 0; n < 2; n++)
		{
			pDX12RenderPass->framebuffersCPUHandles[n] = rtvDesc;
			ThrowIfFailed(m_swapchain->GetBuffer(n, IID_PPV_ARGS(&pDX12RenderPass->framebuffers[n])));
			m_device->CreateRenderTargetView(pDX12RenderPass->framebuffers[n], nullptr, rtvDesc);
			rtvDesc.ptr += rtvDescSize;
		}
	}

	// Update the current framebuffer index.
	m_mainFramebufferIdx = m_swapchain->GetCurrentBackBufferIndex();
}
