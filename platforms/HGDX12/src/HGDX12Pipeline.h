#pragma once

#include "HGDX12.h"
#include "base\src\HGPipeline.h"

namespace hg
{
	namespace Graphics
	{
		class DX12PipelineLayout : public PipelineLayout
		{
		public:
			ID3D12RootSignature* handle;
		};

		class DX12PipelineState : public PipelineState
		{
		public:
			ID3D12PipelineState* handle;
		};
	}
}