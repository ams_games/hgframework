#pragma once

#include "base\src\HGBaseApp.h"

#include "HGDX12.h"
#include "HGDX12Renderer.h"

namespace hg
{
	class WinApp : public BaseApp
	{
	public:
		WinApp(const UINT & width, const UINT & height, const std::wstring &name);

		// Handles setters/getters
		void SetWindow(HWND hWindow) { m_hWindow = hWindow; }
		HWND GetWindow() { return m_hWindow; }
		void SetInstance(HINSTANCE hInstance) { m_hInstance = hInstance; }
		HINSTANCE GetInstance() { return m_hInstance; }

		// Command line args parsing.
		virtual void ParseCmdLineArgs(const char * cmds);

		// Cursor handling.
		virtual void centerCursor() override;
		virtual void trapCursor() override;
		virtual void freeCursor() override;

	private:
		// Platform initialization/destruction.
		virtual bool initPlatform() override;
		virtual bool destroyPlatform() override;

		HWND m_hWindow;
		HINSTANCE m_hInstance;
	};
}