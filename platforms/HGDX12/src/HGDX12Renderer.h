#pragma once

#include "base\src\HGRenderer.h"
#include "HGDX12.h"

namespace hg
{
	namespace Graphics
	{
		class DX12Renderer : public Renderer
		{
		public:
			DX12Renderer();
			~DX12Renderer();
			virtual Shader* LoadShader(
				const LPWSTR& vertexShaderName,
				const LPWSTR& tessControlShaderName,
				const LPWSTR& tessEvaluationShaderName,
				const LPWSTR& geometryShaderName,
				const LPWSTR& fragmentShaderName,
				const LPWSTR& computeShaderName) override;
			virtual void DestroyShader(Shader* pShader) override;
			virtual Texture* LoadTexture(const LPWSTR& textureName) override;
			virtual void DestroyTexture(Texture* pTexture) override;
			virtual Buffer* CreateBuffer(const UINT& bufferSize, const UINT& elementStride, const UINT& bufferUsageFlags, const void* pData = nullptr) override;
			virtual void UpdateBuffer(Buffer const* pBuffer, void const* pData) override;
			virtual void DestroyBuffer(Buffer* pBuffer) override;
			virtual RenderTarget* CreateRenderTarget(const UINT& renderPassIdx,
				const UINT& width, const UINT& height,
				const UINT& format, const float clearColor[4],
				const SampleCount& samples = SampleCount::SC_1, const TextureDimensions& dimensions = TextureDimensions::TD_2D,
				const UINT& layers = 1, const UINT& mips = 1, const bool& sampled = false) override;
			virtual void DestroyRenderTarget(RenderTarget* pRenderTarget) override;
			virtual Sampler* CreateSampler(
				const FilteringMode& minFiltering, const FilteringMode& magFiltering, const FilteringMode& mipFiltering,
				const TextureAddressMode& modeU, const TextureAddressMode& modeV, const TextureAddressMode& modeW,
				const UINT& maxAnisotropy = 1u, const TextureBorderColor& borderColor = FLOAT_OPAQUE_BLACK, const float& mipLODBias = 1.0f,
				const float& minLOD = 0.0f, const float& maxLOD = ~0u, const CompareOp& compareOp = NEVER) override;
			virtual void DestroySampler(Sampler* pSampler) override;
			virtual RenderSubpass* CreateRenderSubpass(const PipelineType& type, const UINT& subpassIdx,
				const UINT& nInputRTs, RenderTarget* const* ppInputRTs,
				const UINT& nOutputRTs, RenderTarget* const* ppOutputRTs, RenderTarget* const* ppResolveRTs,
				const bool& depthWrite, RenderTarget const* pDepthBuffer,
				const UINT& nPreservedRTs, RenderTarget* const* ppPreservedRTs,
				const UINT& nDependencies, UINT const* ppDependencies) override;
			virtual void DestroyRenderSubpass(RenderSubpass* pRenderSubpass) override;
			virtual RenderPass* GetMainRenderPass() override;
			virtual RenderPass* CreateRenderPass(const UINT& width, const UINT& height, const float framebufferClearColor[4], const UINT& nSubpasses = 0, RenderSubpass* const * ppSubpasses = nullptr) override;
			virtual void DestroyRenderPass(RenderPass* pRenderPass) override;
			virtual PipelineLayout* CreatePipelineLayout(const Shader* pShader) override;
			virtual void DestroyPipelineLayout(PipelineLayout* pPipelineLayout) override;
			virtual PipelineState* CreatePipelineState(const Shader* pShader, const PipelineLayout* pPipelineLayout, const RenderPass* pRenderPass, const UINT& subpassIdx, const CullMode& cullMode = NONE) override;
			virtual void DestroyPipelineState(PipelineState* pPipelineState) override;
			virtual CommandRecorder* CreateCommandRecorder(const CommandRecorderType& type) override;
			virtual void DestroyCommandRecorder(CommandRecorder* pCommandRecorder) override;
			virtual CommandBundle* CreateCommandBundle(const RenderPass* pRenderPass, const UINT& subpassIdx) override;
			virtual void DestroyCommandBundle(CommandBundle* pCommandBundle) override;

			virtual void BindTexture(const tinystl::string& name, Texture * pTexture, Sampler * pSampler, PipelineLayout* pPipelineLayout) override;
			virtual void BindTexture(const tinystl::string& name, RenderTarget * pTexture, Sampler * pSampler, PipelineLayout* pPipelineLayout) override;
			virtual void BindSampler(const tinystl::string& name, Sampler * pSampler, PipelineLayout* pPipelineLayout) override;
			virtual void BindBuffer(const tinystl::string& name, Buffer* pBuffer, PipelineLayout* pPipelineLayout) override;

			virtual void ExecuteCommands(unsigned int nCmdRecorders, CommandRecorder* const *ppCommandRecorders) override;
			virtual void FinishFrame() override;
			virtual void Present() override;

			virtual void ResizeMainFramebuffer() override;

		private:
			ID3D12Device2* m_device;
			ComPtr<IDXGISwapChain3> m_swapchain;
			
			ID3D12CommandQueue* m_mainQueue;
			ID3D12CommandAllocator* m_mainCmdAllocator;
			ID3D12CommandAllocator* m_bundleCmdAllocator;

			ID3D12Fence* m_fence;
			HANDLE m_fenceEvent;
			UINT64 m_fenceValue;

			ID3D12DescriptorHeap* m_descHeap;
		};
	}
}