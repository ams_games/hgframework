#pragma once

#include "HGDX12.h"
#include "base\src\HGResources.h"

namespace hg
{
	namespace Graphics
	{
		class DX12Buffer : public Buffer
		{
		public:
			ID3D12Resource* handle;
			D3D12_CONSTANT_BUFFER_VIEW_DESC cbv;
			D3D12_VERTEX_BUFFER_VIEW vbv;
			D3D12_INDEX_BUFFER_VIEW ibv;
			D3D12_STREAM_OUTPUT_BUFFER_VIEW sobv;
		};

		class DX12Texture : public Texture
		{
		public:
			ID3D12Resource* handle;
			D3D12_BUFFER_RTV rtv;
			D3D12_RENDER_TARGET_VIEW_DESC rtvDesc;
			D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc;
			D3D12_BUFFER_SRV srv;
			D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc;
			D3D12_BUFFER_UAV uav;
			D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc;
		};

		class DX12RenderPass : public RenderPass
		{
		public:
			tinystl::vector<ID3D12Resource*> framebuffers;
			tinystl::vector<D3D12_CPU_DESCRIPTOR_HANDLE> framebuffersCPUHandles;
			UINT framebuffersWidth;
			UINT framebuffersHeight;
			float clearColor[4];
		};
	}
}