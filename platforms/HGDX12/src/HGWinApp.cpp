#include "HGWinApp.h"

hg::WinApp::WinApp(const UINT & width, const UINT & height, const std::wstring & name) : BaseApp(width, height, name)
{

}

void hg::WinApp::ParseCmdLineArgs(const char * cmds)
{
	BaseApp::ParseCmdLineArgs(cmds);

	// TODO : Add specific Windows debugging options / modes.
}

bool hg::WinApp::initPlatform()
{
	m_renderer = (static_cast<Graphics::Renderer*>(new Graphics::DX12Renderer()));
	return true;
}

bool hg::WinApp::destroyPlatform()
{
	delete static_cast<Graphics::DX12Renderer*>(m_renderer);
	m_renderer = NULL;
	return true;
}

void hg::WinApp::centerCursor()
{

}

void hg::WinApp::trapCursor()
{
	m_cursorVisible = false;
	ShowCursor(m_cursorVisible);
}

void hg::WinApp::freeCursor()
{
	m_cursorVisible = true;
	ShowCursor(m_cursorVisible);
}
