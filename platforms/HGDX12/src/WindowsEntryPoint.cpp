#include "HGWinApp.h"

extern hg::BaseApp* application;

LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO : Go over all the messages and see if there's somethin else I need to take care of.
	switch (message)
	{
	case WM_CLOSE:
		application->CloseApp();
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	case WM_SIZE:
		application->UpdateWindowSize(LOWORD(lParam), HIWORD(lParam));
		if (!application->IsMaximized() && wParam == SIZE_MAXIMIZED)	{ application->ResizeWindow(); application->SetMaximized(true); }
		else if (application->IsMaximized() && wParam == SIZE_RESTORED) { application->ResizeWindow(); application->SetMaximized(false); }
		return 0;
	case WM_SIZING:
		application->SetResizing(true);
		return 0;
	case WM_EXITSIZEMOVE:
		if (application->IsResizing())
		{
			application->ResizeWindow();
			application->SetResizing(false);
		}
		return 0;
	case WM_KEYDOWN:
		application->OnKey(static_cast<UINT8>(wParam), true);
		return 0;
	case WM_KEYUP:
		application->OnKey(static_cast<UINT8>(wParam), false);
		return 0;
		// TODO : Store mouse position/scroll in the base app?
	case WM_MOUSEMOVE:
		application->OnMouseMove(LOWORD(lParam), HIWORD(lParam));
		return 0;
	case WM_LBUTTONDOWN:
		application->OnMouseButton(LOWORD(lParam), HIWORD(lParam), hg::Input::MouseButton::LEFT_BUTTON, true);
		return 0;
	case WM_LBUTTONUP:
		application->OnMouseButton(LOWORD(lParam), HIWORD(lParam), hg::Input::MouseButton::LEFT_BUTTON, false);
		return 0;
	case WM_RBUTTONDOWN:
		application->OnMouseButton(LOWORD(lParam), HIWORD(lParam), hg::Input::MouseButton::RIGHT_BUTTON, true);
		return 0;
	case WM_RBUTTONUP:
		application->OnMouseButton(LOWORD(lParam), HIWORD(lParam), hg::Input::MouseButton::RIGHT_BUTTON, false);
		return 0;
	case WM_MOUSEWHEEL:
		application->OnMouseWheel(LOWORD(lParam), HIWORD(lParam), GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA);
		return 0;
	}

	// Handle any messages the switch statement didn't.
	return DefWindowProc(hWnd, message, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR lpszCmdLine, int nCmdShow)
{
	// Initialize the window class.
	WNDCLASSEX windowClass = { 0 };
	windowClass.cbSize = sizeof(WNDCLASSEX);
	windowClass.style = CS_HREDRAW | CS_VREDRAW;
	windowClass.lpfnWndProc = WindowProc;
	windowClass.hInstance = hInstance;
	windowClass.hIcon = NULL; // TODO : Add a default icon for the apps.
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.lpszClassName = L"HGFrameworkWindows";
	RegisterClassEx(&windowClass);

	RECT windowRect = { 0, 0, static_cast<LONG>(application->GetWindowWidth()), static_cast<LONG>(application->GetWindowHeight()) };
	AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, FALSE);

	// Create the window and store a handle to it.
	HWND window;
	window = CreateWindow(
		L"HGFrameworkWindows",
		(WCHAR*)application->GetName().c_str(),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		nullptr,		// We have no parent window.
		nullptr,		// We aren't using menus.
		hInstance,
		application);

	// Set the window, instance and cursor handles for the application.
	(static_cast<hg::WinApp*>(application))->SetWindow(window);
	(static_cast<hg::WinApp*>(application))->SetInstance(hInstance);

	// Display the window.
	ShowWindow(window, nCmdShow);

	// Parse the command line parameters
	application->ParseCmdLineArgs(static_cast<char*>(lpszCmdLine));

	// Run the main application loop
	return application->RunApp();
}

