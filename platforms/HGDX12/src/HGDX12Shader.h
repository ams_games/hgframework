#pragma once

#include "HGDX12.h"
#include "base\src\HGShader.h"

namespace hg
{
	namespace Graphics
	{
		struct ShaderData
		{
			tinystl::vector<UINT> source;
			ID3DBlob* blob;
		};

		class DX12Shader : public Shader
		{
		public:
			ShaderData vertexStage;
			ShaderData tesselationControlStage;
			ShaderData tesselationEvaluationStage;
			ShaderData geometryStage;
			ShaderData fragmentStage;
			ShaderData computeStage;
		};
	}
}