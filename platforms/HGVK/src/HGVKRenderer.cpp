#include "HGVKRenderer.h"

#include "HGVKUtils.h"
#include "HGWinApp.h"
#include <fstream>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <spirv_glsl.hpp>

// Useful for gettin messages on release.
//#define FORCE_DEBUG

extern hg::BaseApp* application;

void hg::Graphics::VKRenderer::initInstance()
{
	// Enummerate supported instance layers and extensions.
	uint32_t nSupportedInstanceLayers;
	ThrowIfFailed(vk::enumerateInstanceLayerProperties(&nSupportedInstanceLayers, NULL));
	tinystl::vector<vk::LayerProperties> supportedInstanceLayers(nSupportedInstanceLayers);
	ThrowIfFailed(vk::enumerateInstanceLayerProperties(&nSupportedInstanceLayers, supportedInstanceLayers.data()));
	uint32_t nSupportedInstanceExtensions;
	ThrowIfFailed(vk::enumerateInstanceExtensionProperties(NULL, &nSupportedInstanceExtensions, NULL));
	tinystl::vector<vk::ExtensionProperties> supportedInstanceExtensions(nSupportedInstanceExtensions);
	ThrowIfFailed(enumerateInstanceExtensionProperties(NULL, &nSupportedInstanceExtensions, supportedInstanceExtensions.data()));

	// Select the proper instance layers and extensions.
	tinystl::vector<const char *> instanceLayers;
	tinystl::vector<const char *> instanceExtensions;
	if (ValidateVKExtension(VK_KHR_SURFACE_EXTENSION_NAME, &supportedInstanceExtensions)) instanceExtensions.push_back(VK_KHR_SURFACE_EXTENSION_NAME);
#if defined(_WIN32)
	if (ValidateVKExtension(VK_KHR_WIN32_SURFACE_EXTENSION_NAME, &supportedInstanceExtensions)) instanceExtensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
#else
	// TODO : Add surface support for other platforms.
#endif
#if defined(_DEBUG ) || defined(FORCE_DEBUG)
	if (ValidateVKExtension(VK_EXT_DEBUG_REPORT_EXTENSION_NAME, &supportedInstanceExtensions)) instanceExtensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
	if (ValidateVKLayer("VK_LAYER_LUNARG_standard_validation", &supportedInstanceLayers)) instanceLayers.push_back("VK_LAYER_LUNARG_standard_validation");
#endif

	// Get the application name.
	char applicationName[256];
	size_t nCharsConverted;
	wcstombs_s(&nCharsConverted, applicationName, application->GetName().c_str(), 256);

	// Create the instance
	vk::ApplicationInfo applicationInfo(applicationName, 1, "HGFramework", 1, VK_API_VERSION_1_0);
	vk::InstanceCreateInfo instanceInfo(vk::InstanceCreateFlags(), &applicationInfo, 
		static_cast<uint32_t>(instanceLayers.size()), instanceLayers.data(), 
		static_cast<uint32_t>(instanceExtensions.size()), instanceExtensions.data());
	ThrowIfFailed(createInstance(&instanceInfo, NULL, &m_instance));

	// Create the surface
#if defined(_WIN32)
	vk::Win32SurfaceCreateInfoKHR surfaceInfo(vk::Win32SurfaceCreateFlagsKHR(), ((WinApp*)application)->GetInstance(), ((WinApp*)application)->GetWindow());
	ThrowIfFailed(m_instance.createWin32SurfaceKHR(&surfaceInfo, NULL, &m_surface));
#else
#endif
}

void hg::Graphics::VKRenderer::initDebugCallback()
{
	VkDebugReportCallbackCreateInfoEXT debugCallbackInfo = {};
	debugCallbackInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
	debugCallbackInfo.flags =	VK_DEBUG_REPORT_INFORMATION_BIT_EXT | 
								VK_DEBUG_REPORT_WARNING_BIT_EXT | 
								VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT | 
								VK_DEBUG_REPORT_ERROR_BIT_EXT | 
								VK_DEBUG_REPORT_DEBUG_BIT_EXT;
	debugCallbackInfo.pfnCallback = debugCallback;

	auto func = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(m_instance, "vkCreateDebugReportCallbackEXT");
	if (func != nullptr) {
		ThrowIfFailed(func(m_instance, &debugCallbackInfo, NULL, &m_debugCallback));
	}
	else {
		throw std::exception();
	}
}

void hg::Graphics::VKRenderer::initPhysicalDevice()
{
	// Enumerate the available physical devices.
	uint32_t nPhysicalDevices;
	ThrowIfFailed(m_instance.enumeratePhysicalDevices(&nPhysicalDevices, NULL));
	tinystl::vector<vk::PhysicalDevice> physicalDevices(nPhysicalDevices);
	ThrowIfFailed(m_instance.enumeratePhysicalDevices(&nPhysicalDevices, physicalDevices.data()));

	// Chose a physical device to work with.
	// TODO : Choose the best available device.
	// TODO : Multi-device.
	m_gpu = physicalDevices[0];

	// Get physical device properties and features.
	m_gpu.getProperties(&m_gpuProperties);
	m_gpu.getMemoryProperties(&m_gpuMemoryProperties);
	m_gpu.getFeatures(&m_gpuFeatures);

	// Get the queue family properties.
	uint32_t nQueueFamilyProperties;
	m_gpu.getQueueFamilyProperties(&nQueueFamilyProperties, NULL);
	tinystl::vector<vk::QueueFamilyProperties> queueFamilyProperties(nQueueFamilyProperties);
	m_gpu.getQueueFamilyProperties(&nQueueFamilyProperties, queueFamilyProperties.data());

	// Iterate over each queue family to learn whether it supports presenting.
	tinystl::vector<vk::Bool32> presentSupported(nQueueFamilyProperties);
	for (uint32_t i = 0; i < nQueueFamilyProperties; i++)
		ThrowIfFailed(m_gpu.getSurfaceSupportKHR(i, m_surface, &presentSupported[i]));

	// Search for a queue that support at least graphics commands and presentation.
	for (uint32_t i = 0; i < nQueueFamilyProperties; ++i) 
	{
		if ((queueFamilyProperties[i].queueFlags & vk::QueueFlagBits::eGraphics) && (presentSupported[i] == VK_TRUE))
		{
			m_mainQueueFamilyIdx = i;
			break;
		}
	}
}

void hg::Graphics::VKRenderer::initLogicalDevice()
{
	// Enummerate supported device layers and extensions.
	uint32_t nSupportedDeviceLayers;
	ThrowIfFailed(m_gpu.enumerateDeviceLayerProperties(&nSupportedDeviceLayers, NULL));
	tinystl::vector<vk::LayerProperties> supportedDeviceLayers(nSupportedDeviceLayers);
	ThrowIfFailed(m_gpu.enumerateDeviceLayerProperties(&nSupportedDeviceLayers, supportedDeviceLayers.data()));
	uint32_t nSupportedDeviceExtensions;
	ThrowIfFailed(m_gpu.enumerateDeviceExtensionProperties(NULL, &nSupportedDeviceExtensions, NULL));
	tinystl::vector<vk::ExtensionProperties> supportedDeviceExtensions(nSupportedDeviceExtensions);
	ThrowIfFailed(m_gpu.enumerateDeviceExtensionProperties(NULL, &nSupportedDeviceExtensions, supportedDeviceExtensions.data()));

	// Select the proper device extensions.
	tinystl::vector<const char *> deviceExtensions;
	if (ValidateVKExtension(VK_KHR_SWAPCHAIN_EXTENSION_NAME, &supportedDeviceExtensions)) deviceExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

	// Create the logical device.
	// TODO : Figure out how to properly handle multiple queue families.
	float mainQueuePriority[1] = { 1.0f };
	vk::DeviceQueueCreateInfo deviceQueue(vk::DeviceQueueCreateFlags(), m_mainQueueFamilyIdx, 1, mainQueuePriority);
	vk::DeviceCreateInfo deviceInfo(vk::DeviceCreateFlags(), 1, &deviceQueue, 0, NULL,
		static_cast<uint32_t>(deviceExtensions.size()), deviceExtensions.data(), &m_gpuFeatures);
	ThrowIfFailed(m_gpu.createDevice(&deviceInfo, NULL, &m_device));

	// Get the actual main queues.
	m_device.getQueue(m_mainQueueFamilyIdx, 0, &m_mainQueue);

	// Create the main command pool (command allocator in DX12).
	vk::CommandPoolCreateInfo cmdPoolInfo(vk::CommandPoolCreateFlags(), m_mainQueueFamilyIdx);
	ThrowIfFailed(m_device.createCommandPool(&cmdPoolInfo, NULL, &m_commandPool));

	// Create a command bundle pool.
	vk::CommandPoolCreateInfo bundlePoolInfo(vk::CommandPoolCreateFlags(), m_mainQueueFamilyIdx);
	ThrowIfFailed(m_device.createCommandPool(&bundlePoolInfo, NULL, &m_bundlePool));

	// Create a copy command pool and command buffer, use to load resources from memory.
	vk::CommandPoolCreateInfo copyPoolInfo(vk::CommandPoolCreateFlagBits::eTransient, m_mainQueueFamilyIdx);
	ThrowIfFailed(m_device.createCommandPool(&copyPoolInfo, NULL, &m_copyCmdPool));
	vk::CommandBufferAllocateInfo copyBufferInfo(m_copyCmdPool, vk::CommandBufferLevel::ePrimary, 1);
	ThrowIfFailed(m_device.allocateCommandBuffers(&copyBufferInfo, &m_copyCmdBuffer));

	// Create the needed synchronization primitives.
	ThrowIfFailed(m_device.createSemaphore(&vk::SemaphoreCreateInfo(), nullptr, &m_semaphore));
	ThrowIfFailed(m_device.createFence(&vk::FenceCreateInfo(), nullptr, &m_fence));
}

void hg::Graphics::VKRenderer::initSwapchain()
{
	// Get the surface capabilities.
	ThrowIfFailed(m_gpu.getSurfaceCapabilitiesKHR(m_surface, &m_surfaceCapabilities));

	// TODO : Support Triple Buffering (PresentModeKHR:eMailbox).

	// Create the swapchain.
	vk::SwapchainCreateInfoKHR swapchainInfo(vk::SwapchainCreateFlagsKHR(), m_surface, 2,
		vk::Format::eR8G8B8A8Srgb, vk::ColorSpaceKHR::eSrgbNonlinear, 
		m_surfaceCapabilities.currentExtent, 1, vk::ImageUsageFlagBits::eColorAttachment, 
		vk::SharingMode::eExclusive, 0, NULL, m_surfaceCapabilities.currentTransform, 
		vk::CompositeAlphaFlagBitsKHR::eOpaque, vk::PresentModeKHR::eFifo, true, nullptr);
	ThrowIfFailed(m_device.createSwapchainKHR(&swapchainInfo, NULL, &m_swapchain));

	// Get the backbuffer handles
	uint32_t nSwapchainImages;
	ThrowIfFailed(m_device.getSwapchainImagesKHR(m_swapchain, &nSwapchainImages, NULL));
	tinystl::vector<vk::Image> swapchainImages(nSwapchainImages);
	ThrowIfFailed(m_device.getSwapchainImagesKHR(m_swapchain, &nSwapchainImages, swapchainImages.data()));

	// Store the image handles.
	for (uint32_t i = 0; i < m_surfaceCapabilities.minImageCount; i++) {
		m_mainFramebuffers[i] = swapchainImages[i];
	}

	// Create the color attachments image views
	for (uint32_t i = 0; i < m_surfaceCapabilities.minImageCount; i++) {
		vk::ImageViewCreateInfo imageViewInfo(
			vk::ImageViewCreateFlags(), m_mainFramebuffers[i], vk::ImageViewType::e2D, vk::Format::eR8G8B8A8Srgb,
			vk::ComponentMapping(), vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1));
		ThrowIfFailed(m_device.createImageView(&imageViewInfo, NULL, &m_mainFramebufferViews[i]));
	}

	// Add the final framebuffer's color buffer attachment description.
	if (m_attachmentsMap.empty())
	{
		m_attachmentsMap.insert(tinystl::pair <UINT, tinystl::vector<vk::AttachmentDescription>>(~0u, 0));
		m_attachmentsMap[MAIN_RENDERPASS_IDX].push_back(vk::AttachmentDescription(vk::AttachmentDescriptionFlags(), vk::Format::eR8G8B8A8Srgb,
			vk::SampleCountFlagBits::e1, vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eStore, vk::AttachmentLoadOp::eDontCare,
			vk::AttachmentStoreOp::eDontCare, vk::ImageLayout::eUndefined, vk::ImageLayout::ePresentSrcKHR));

		m_attachmentViewsMap.insert(tinystl::pair <UINT, tinystl::vector<vk::ImageView>>(~0u, 0));
		m_attachmentViewsMap[MAIN_RENDERPASS_IDX].push_back(vk::ImageView()); // We'll change this on framebuffer creation.

		m_mainFramebufferAttachmentRef = vk::AttachmentReference(0u, vk::ImageLayout::eColorAttachmentOptimal);
	}

	// Get the index of the next available swapchain image
	ThrowIfFailed(m_device.acquireNextImageKHR(m_swapchain, UINT64_MAX, m_semaphore, nullptr, &m_mainFramebufferIdx));
}

void hg::Graphics::VKRenderer::initMainRenderPass()
{
	// Init some main render pass parameters.
	m_mainRenderPass = new VKRenderPass();
	for (UINT i = 0; i < 4; i++)
		m_mainRenderPass->framebufferClearColor[i] = 0.0f;
	m_mainRenderPass->renderPassIdx = MAIN_RENDERPASS_IDX;
	m_mainRenderPass->framebufferWidth = m_surfaceCapabilities.currentExtent.width;
	m_mainRenderPass->framebufferHeight = m_surfaceCapabilities.currentExtent.height;
	m_mainRenderPass->subpasses.push_back(vk::SubpassDescription(vk::SubpassDescriptionFlags(), vk::PipelineBindPoint::eGraphics,
		0u, NULL, 1u, &m_mainFramebufferAttachmentRef, NULL, NULL, 0u, NULL));

	// Create the main render pass.
	vk::RenderPassCreateInfo renderPassInfo(vk::RenderPassCreateFlags(),
		1u, m_attachmentsMap[MAIN_RENDERPASS_IDX].data(),
		1u, m_mainRenderPass->subpasses.data(),
		0u, nullptr);
	ThrowIfFailed(m_device.createRenderPass(&renderPassInfo, NULL, &m_mainRenderPass->handle));

	// Create the main framebuffers.
	m_mainRenderPass->framebuffers.resize(BACKBUFFER_COUNT);
	m_attachmentViewsMap[MAIN_RENDERPASS_IDX][0] = m_mainFramebufferViews[0];
	vk::FramebufferCreateInfo framebufferInfo(vk::FramebufferCreateFlags(), m_mainRenderPass->handle,
		1, m_attachmentViewsMap[MAIN_RENDERPASS_IDX].data(), m_mainRenderPass->framebufferWidth, m_mainRenderPass->framebufferHeight, 1);
	ThrowIfFailed(m_device.createFramebuffer(&framebufferInfo, NULL, &m_mainRenderPass->framebuffers[0]));
	m_attachmentViewsMap[MAIN_RENDERPASS_IDX][0] = m_mainFramebufferViews[1];
	ThrowIfFailed(m_device.createFramebuffer(&framebufferInfo, NULL, &m_mainRenderPass->framebuffers[1]));
}

hg::Graphics::VKRenderer::VKRenderer()
{
	initInstance();
#if defined(_DEBUG ) || defined(FORCE_DEBUG)
	initDebugCallback();
#endif
	initPhysicalDevice();
	initLogicalDevice();
	initSwapchain();
	initMainRenderPass();
}

hg::Graphics::VKRenderer::~VKRenderer()
{
	// Destroy the main render pass.
	DestroyRenderPass(m_mainRenderPass);

	// Destroy the framebuffer's color attchaments.
	m_device.destroyImageView(m_mainFramebufferViews[0], NULL);
	m_device.destroyImageView(m_mainFramebufferViews[1], NULL);

	// Destroy the swapchain.
	m_device.destroySwapchainKHR(m_swapchain, NULL);

	// Destroy the copy command buffer.
	m_device.freeCommandBuffers(m_copyCmdPool, 1, &m_copyCmdBuffer);

	// Destroy the command pools.
	m_device.destroyCommandPool(m_commandPool, NULL);
	m_device.destroyCommandPool(m_bundlePool, NULL);
	m_device.destroyCommandPool(m_copyCmdPool, NULL);

	// Destroy the synchronization primitives.
	m_device.destroyFence(m_fence, NULL);
	m_device.destroySemaphore(m_semaphore, NULL);

	// Destroy the logical device.
	m_device.destroy(NULL);

	// Destroy the window.
	m_instance.destroySurfaceKHR(m_surface, NULL);
	DestroyWindow(((WinApp*)application)->GetWindow());

	// Destroy the debug callback.
#if defined(_DEBUG ) || defined(FORCE_DEBUG)
	auto func = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(m_instance, "vkDestroyDebugReportCallbackEXT");
	if (func != nullptr) {
		func(m_instance, m_debugCallback, NULL);
	}
#endif

	// Destroy the instance.
	m_instance.destroy(NULL);
}

hg::Graphics::Shader * hg::Graphics::VKRenderer::LoadShader(const LPWSTR & vertexShaderName, const LPWSTR & tessControlShaderName, const LPWSTR & tessEvaluationShaderName, const LPWSTR & geometryShaderName, const LPWSTR & fragmentShaderName, const LPWSTR & computeShaderName)
{
	VKShader* shader = new VKShader();

	// Look for the compiled shaders path
	WCHAR compiledShadersPath[512];
	DWORD size = GetModuleFileName(nullptr, compiledShadersPath, 512);
	if (size == 0 || size == 512)
	{
		// Method failed or path was truncated.
		throw std::exception();
	}
	WCHAR* lastSlash = wcsrchr(compiledShadersPath, L'\\');
	if (lastSlash)
	{
		wcscpy_s(lastSlash, sizeof(L"\\shaders\\") / _WCHAR_T_SIZE, L"\\shaders\\");
	}

	// Create all the shader modules from the compiled files.
	std::wstring inputFiles[SHADER_STAGES_COUNT] = {
		std::wstring(vertexShaderName) + L".vert.spv" ,
		std::wstring(tessControlShaderName) + L".tesc.spv" ,
		std::wstring(tessEvaluationShaderName) + L".tese.spv" ,
		std::wstring(geometryShaderName) + L".geom.spv" ,
		std::wstring(fragmentShaderName) + L".frag.spv" ,
		std::wstring(computeShaderName) + L".comp.spv" };
	ShaderData* outputShaders[SHADER_STAGES_COUNT] = { &shader->vertexStage, &shader->tesselationControlStage,
		&shader->tesselationEvaluationStage, &shader->geometryStage, &shader->fragmentStage,&shader->computeStage };

	for (UINT i = 0; i < SHADER_STAGES_COUNT; i++)
	{
		std::ifstream file(compiledShadersPath + inputFiles[i], std::ifstream::binary);
		if (file.is_open())
		{
			// Get length of the shader.
			file.seekg(0, file.end);
			UINT fileSize = static_cast<UINT>(file.tellg());
			file.seekg(0, file.beg);

			// Resize the source vector to hold enough values.
			outputShaders[i]->source.resize(fileSize / sizeof(UINT));

			// Read into the vector.
			file.read(reinterpret_cast<char *>(outputShaders[i]->source.data()), fileSize);
			file.close();

			vk::ShaderModuleCreateInfo shaderModuleInfo(vk::ShaderModuleCreateFlags(), outputShaders[i]->source.size() * sizeof(UINT), outputShaders[i]->source.data());
			ThrowIfFailed(m_device.createShaderModule(&shaderModuleInfo, NULL, &outputShaders[i]->module));
		}
	}

	return shader;
}

void hg::Graphics::VKRenderer::DestroyShader(Shader * pShader)
{
	VKShader* pVKShader = static_cast<VKShader*>(pShader);

	m_device.destroyShaderModule(pVKShader->vertexStage.module, NULL);
	m_device.destroyShaderModule(pVKShader->fragmentStage.module, NULL);

	delete pShader;
	pShader = nullptr;
}

hg::Graphics::Texture * hg::Graphics::VKRenderer::LoadTexture(const LPWSTR & textureName)
{
	VKTexture* texture = new VKTexture();

	// Look for the textures path
	std::wstring path;
	{
		WCHAR texturesPath[512];
		DWORD size = GetModuleFileName(nullptr, texturesPath, 512);
		if (size == 0 || size == 512)
		{
			// Method failed or path was truncated.
			throw std::exception();
		}

		WCHAR* lastSlash = wcsrchr(texturesPath, L'\\');
		if (lastSlash)
		{
			wcscpy_s(lastSlash, sizeof(L"\\textures\\") / _WCHAR_T_SIZE, L"\\textures\\");
		}

		path += texturesPath;
	}
	path += textureName;

	char texPath[512];
	size_t nCharsConverted;
	wcstombs_s(&nCharsConverted, texPath, path.c_str(), 512);

	// Load the texture using stb_image.
	int texWidth, texHeight, texChannels;
	stbi_uc* pixels = stbi_load(texPath, &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
	VkDeviceSize imageSize = texWidth * texHeight * 4;

	if (!pixels) {
		throw std::runtime_error("failed to load texture image!");
	}

	// Create a staging buffer and map the memory to it.
	vk::Buffer stagingBuffer;
	vk::BufferCreateInfo stagingBufferInfo(vk::BufferCreateFlags(), imageSize,
		vk::BufferUsageFlagBits::eTransferSrc, vk::SharingMode::eExclusive, 0, NULL);
	ThrowIfFailed(m_device.createBuffer(&stagingBufferInfo, NULL, &stagingBuffer));

	vk::DeviceMemory stagingBufferMemory;
	vk::MemoryRequirements stagingBufferMemoryRequirements;
	m_device.getBufferMemoryRequirements(stagingBuffer, &stagingBufferMemoryRequirements);
	vk::MemoryAllocateInfo bufferAllocInfo(stagingBufferMemoryRequirements.size, GetMemoryTypeFromProperties(
		stagingBufferMemoryRequirements.memoryTypeBits, m_gpuMemoryProperties,
		vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent));
	ThrowIfFailed(m_device.allocateMemory(&bufferAllocInfo, NULL, &stagingBufferMemory));

	UCHAR* pMappedData;
	ThrowIfFailed(m_device.mapMemory(stagingBufferMemory, 0, stagingBufferMemoryRequirements.size, vk::MemoryMapFlags(), (void**)&pMappedData));
	memcpy(pMappedData, pixels, static_cast<size_t>(imageSize));
	m_device.unmapMemory(stagingBufferMemory);

	m_device.bindBufferMemory(stagingBuffer, stagingBufferMemory, 0);

	// Release the buffer allocated when loading the image.
	stbi_image_free(pixels);

	// Create an image to store the buffer data.
	vk::ImageCreateInfo imageInfo(vk::ImageCreateFlags(), vk::ImageType::e2D, vk::Format::eR8G8B8A8Unorm,
	vk::Extent3D(texWidth, texHeight, 1), 1, 1, vk::SampleCountFlagBits::e1, 
	vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled, 
	vk::SharingMode::eExclusive, 0, NULL, vk::ImageLayout::eUndefined);
	ThrowIfFailed(m_device.createImage(&imageInfo, NULL, &texture->handle));

	vk::MemoryRequirements imageMemoryRequirements;
	m_device.getImageMemoryRequirements(texture->handle, &imageMemoryRequirements);
	vk::MemoryAllocateInfo imageAllocInfo(imageMemoryRequirements.size, GetMemoryTypeFromProperties(
		imageMemoryRequirements.memoryTypeBits, m_gpuMemoryProperties, vk::MemoryPropertyFlagBits::eDeviceLocal));
	ThrowIfFailed(m_device.allocateMemory(&imageAllocInfo, NULL, &texture->memory));

	m_device.bindImageMemory(texture->handle, texture->memory, 0);

	// Copy the buffer contents to the image.
	vk::CommandBufferBeginInfo copyBeginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit, NULL);
	m_copyCmdBuffer.begin(&copyBeginInfo);

	vk::ImageMemoryBarrier copyDstBarrier(
		vk::AccessFlags(), vk::AccessFlagBits::eTransferWrite, 
		vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal, 
		VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED, 
		texture->handle, vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1));
	m_copyCmdBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTopOfPipe, vk::PipelineStageFlagBits::eTransfer, vk::DependencyFlags(), 0, nullptr, 0, nullptr, 1, &copyDstBarrier);

	vk::BufferImageCopy copyRegion(0u, 0u, 0u, vk::ImageSubresourceLayers(vk::ImageAspectFlagBits::eColor, 0, 0, 1),
	vk::Offset3D(0, 0, 0), vk::Extent3D(texWidth, texHeight,1));
	m_copyCmdBuffer.copyBufferToImage(stagingBuffer, texture->handle, vk::ImageLayout::eTransferDstOptimal, copyRegion);

	vk::ImageMemoryBarrier shaderReadBarrier(
		vk::AccessFlagBits::eTransferWrite, vk::AccessFlagBits::eShaderRead,
		vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal,
		VK_QUEUE_FAMILY_IGNORED, VK_QUEUE_FAMILY_IGNORED,
		texture->handle, vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1));
	m_copyCmdBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, vk::DependencyFlags(), 0, nullptr, 0, nullptr, 1, &shaderReadBarrier);

	m_copyCmdBuffer.end();

	vk::SubmitInfo copySubmitInfo(0, NULL, NULL, 1, &m_copyCmdBuffer, 0u, NULL);
	m_mainQueue.submit(1, &copySubmitInfo, nullptr);
	m_mainQueue.waitIdle();

	// Create an image view for the texture.
	vk::ImageViewCreateInfo viewInfo(
		vk::ImageViewCreateFlags(), texture->handle, vk::ImageViewType::e2D,
		vk::Format::eR8G8B8A8Unorm, vk::ComponentMapping(),
		vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1));
	ThrowIfFailed(m_device.createImageView(&viewInfo, NULL, &texture->view));

	// Free the allocated resources.
	m_device.destroyBuffer(stagingBuffer, NULL);
	m_device.freeMemory(stagingBufferMemory, NULL);

	return texture;
}

void hg::Graphics::VKRenderer::DestroyTexture(Texture * pTexture)
{
	VKTexture* pVKTexture = static_cast<VKTexture*>(pTexture);

	m_device.destroyImageView(pVKTexture->view, NULL);
	m_device.destroyImage(pVKTexture->handle, NULL);
	m_device.freeMemory(pVKTexture->memory, NULL);

	delete pTexture;
	pTexture = nullptr;
}

hg::Graphics::Buffer * hg::Graphics::VKRenderer::CreateBuffer(const UINT & bufferSize, const UINT & elementStride, const UINT & bufferUsageFlags, const void * pData)
{
	VKBuffer* buffer = new VKBuffer();
	buffer->size = bufferSize;

	// Create the buffer
	vk::BufferCreateInfo vertexBufferInfo(vk::BufferCreateFlags(), bufferSize,
		vk::BufferUsageFlagBits(bufferUsageFlags), vk::SharingMode::eExclusive, 0, NULL);
	ThrowIfFailed(m_device.createBuffer(&vertexBufferInfo, NULL, &buffer->handle));

	// Allocate memory
	vk::MemoryRequirements bufferMemoryRequirements;
	m_device.getBufferMemoryRequirements(buffer->handle, &bufferMemoryRequirements);
	vk::MemoryAllocateInfo bufferAllocInfo(bufferMemoryRequirements.size, GetMemoryTypeFromProperties(
		bufferMemoryRequirements.memoryTypeBits, m_gpuMemoryProperties,
		vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent));
	ThrowIfFailed(m_device.allocateMemory(&bufferAllocInfo, NULL, &buffer->memory));

	// Map the data (if providen)
	if (pData)
	{
		UINT8* pMappedData;
		ThrowIfFailed(m_device.mapMemory(buffer->memory, 0, bufferMemoryRequirements.size, vk::MemoryMapFlags(), (void **)&pMappedData));
		memcpy(pMappedData, pData, bufferSize);
		m_device.unmapMemory(buffer->memory);
	}

	// Bind the memory to the buffer
	m_device.bindBufferMemory(buffer->handle, buffer->memory, 0);

	return buffer;
}

void hg::Graphics::VKRenderer::UpdateBuffer(Buffer const * pBuffer, void const * pData)
{
	VKBuffer const* pVKBuffer = static_cast<VKBuffer const*>(pBuffer);

	// Get the buffer memory requirements.
	// NOTE : Is this necessary?
	vk::MemoryRequirements bufferMemoryRequirements;
	m_device.getBufferMemoryRequirements(pVKBuffer->handle, &bufferMemoryRequirements);

	// Copy the provided data into the buffer.
	UINT8* pMappedData;
	ThrowIfFailed(m_device.mapMemory(pVKBuffer->memory, 0, bufferMemoryRequirements.size, vk::MemoryMapFlags(), (void **)&pMappedData));
	memcpy(pMappedData, pData, pVKBuffer->size);
	m_device.unmapMemory(pVKBuffer->memory);
}

void hg::Graphics::VKRenderer::DestroyBuffer(Buffer * pBuffer)
{
	VKBuffer* pVKBuffer = static_cast<VKBuffer*>(pBuffer);

	//m_device.destroyBufferView(pVKBuffer->view, NULL);
	m_device.destroyBuffer(pVKBuffer->handle, NULL);
	m_device.freeMemory(pVKBuffer->memory, NULL);

	delete pBuffer;
	pBuffer = nullptr;
}

hg::Graphics::RenderTarget * hg::Graphics::VKRenderer::CreateRenderTarget(const UINT & renderPassIdx, const UINT & width, const UINT & height, const UINT & format, const float clearColor[4], const SampleCount & samples, const TextureDimensions & dimensions, const UINT & layers, const UINT & mips, const bool & sampled)
{
	VKRenderTarget* renderTarget = new VKRenderTarget();
	for (UINT i = 0; i < 4; i++)
		renderTarget->clearColor[i] = clearColor[i];

	renderTarget->width = width;
	renderTarget->height = height;

	bool isDepth = static_cast<vk::Format>(format) == vk::Format::eD32Sfloat;

	// Create the image resource.
	vk::ImageCreateInfo imageInfo(
		vk::ImageCreateFlags(), static_cast<vk::ImageType>(dimensions), static_cast<vk::Format>(format),
		vk::Extent3D(width, height, 1), mips, layers, static_cast<vk::SampleCountFlagBits>(samples), vk::ImageTiling::eOptimal,
		vk::ImageUsageFlagBits::eInputAttachment |
		(isDepth ? vk::ImageUsageFlagBits::eDepthStencilAttachment : vk::ImageUsageFlagBits::eColorAttachment) |
		(sampled ? vk::ImageUsageFlagBits::eSampled : vk::ImageUsageFlagBits(0u)),
		vk::SharingMode::eExclusive, 0, NULL, vk::ImageLayout::eUndefined);
	ThrowIfFailed(m_device.createImage(&imageInfo, NULL, &renderTarget->handle));

	// Allocate the necessary memory.
	vk::MemoryRequirements memRequiremnts;
	m_device.getImageMemoryRequirements(renderTarget->handle, &memRequiremnts);
	vk::MemoryAllocateInfo imageAlloc(memRequiremnts.size, GetMemoryTypeFromProperties(
		memRequiremnts.memoryTypeBits, m_gpuMemoryProperties, vk::MemoryPropertyFlagBits::eDeviceLocal));
	ThrowIfFailed(m_device.allocateMemory(&imageAlloc, NULL, &renderTarget->memory));

	// Bind the allocated memory to the image.
	m_device.bindImageMemory(renderTarget->handle, renderTarget->memory, 0);

	// Create the image view.
	vk::ImageViewCreateInfo viewInfo(
		vk::ImageViewCreateFlags(), renderTarget->handle, static_cast<vk::ImageViewType>(dimensions + ((layers > 1) && (dimensions != TextureDimensions::TD_3D) ? 4 : 0)),
		static_cast<vk::Format>(format), vk::ComponentMapping(), vk::ImageSubresourceRange(
			isDepth ? vk::ImageAspectFlagBits::eDepth : vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1));
	ThrowIfFailed(m_device.createImageView(&viewInfo, NULL, &renderTarget->view));

	// Populate the attachment description.
	if (!m_attachmentsMap.find(renderPassIdx).node)
	{
		m_attachmentsMap.insert(tinystl::pair <UINT, tinystl::vector<vk::AttachmentDescription>>(renderPassIdx, 0));
		m_attachmentViewsMap.insert(tinystl::pair <UINT, tinystl::vector<vk::ImageView>>(renderPassIdx, 0));
	}
	renderTarget->attachmentIdx = static_cast<UINT>(m_attachmentsMap[renderPassIdx].size());
	m_attachmentsMap[renderPassIdx].push_back(vk::AttachmentDescription(vk::AttachmentDescriptionFlags(),
		static_cast<vk::Format>(format), static_cast<vk::SampleCountFlagBits>(samples),
		vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eStore,
		vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
		vk::ImageLayout::eUndefined, sampled ? vk::ImageLayout::eShaderReadOnlyOptimal : (isDepth ? vk::ImageLayout::eDepthStencilAttachmentOptimal : vk::ImageLayout::eColorAttachmentOptimal)));
	m_attachmentViewsMap[renderPassIdx].push_back(renderTarget->view);

	return renderTarget;
}

void hg::Graphics::VKRenderer::DestroyRenderTarget(RenderTarget * pRenderTarget)
{
	VKRenderTarget* pVKRenderTarget = static_cast<VKRenderTarget*>(pRenderTarget);

	m_device.destroyImageView(pVKRenderTarget->view, NULL);
	m_device.destroyImage(pVKRenderTarget->handle, NULL);
	m_device.freeMemory(pVKRenderTarget->memory, NULL);

	delete pRenderTarget;
	pRenderTarget = nullptr;
}

hg::Graphics::Sampler * hg::Graphics::VKRenderer::CreateSampler(const FilteringMode & minFiltering, const FilteringMode & magFiltering, const FilteringMode & mipFiltering, const TextureAddressMode & modeU, const TextureAddressMode & modeV, const TextureAddressMode & modeW, const UINT & maxAnisotropy, const TextureBorderColor & borderColor, const float & mipLODBias, const float & minLOD, const float & maxLOD, const CompareOp & compareOp)
{
	VKSampler* sampler = new VKSampler();

	vk::SamplerCreateInfo samplerInfo(vk::SamplerCreateFlags(),
		static_cast<vk::Filter>(magFiltering), static_cast<vk::Filter>(minFiltering),
		mipFiltering == FilteringMode::POINT ? vk::SamplerMipmapMode::eNearest : vk::SamplerMipmapMode::eLinear,
		static_cast<vk::SamplerAddressMode>(modeU), static_cast<vk::SamplerAddressMode>(modeV), static_cast<vk::SamplerAddressMode>(modeW),
		mipLODBias, VK_TRUE, static_cast<float>(maxAnisotropy), compareOp != CompareOp::NEVER, static_cast<vk::CompareOp>(compareOp),
		minLOD, maxLOD, static_cast<vk::BorderColor>(borderColor), VK_FALSE);
	ThrowIfFailed(m_device.createSampler(&samplerInfo, NULL, &sampler->handle));

	return sampler;
}

void hg::Graphics::VKRenderer::DestroySampler(Sampler * pSampler)
{
	VKSampler* pVKSampler = static_cast<VKSampler*>(pSampler);

	m_device.destroySampler(pVKSampler->handle, NULL);

	delete pSampler;
	pSampler = nullptr;
}

hg::Graphics::RenderSubpass * hg::Graphics::VKRenderer::CreateRenderSubpass(const PipelineType & type, const UINT & subpassIdx, const UINT & nInputRTs, RenderTarget * const * ppInputRTs, const UINT & nOutputRTs, RenderTarget * const * ppOutputRTs, RenderTarget * const * ppResolveRTs, const bool & depthWrite, RenderTarget const * pDepthBuffer, const UINT & nPreservedRTs, RenderTarget * const * ppPreservedRTs, const UINT & nDependencies, UINT const * ppDependencies)
{
	VKRenderSubpass* renderSubpass = new VKRenderSubpass();
	renderSubpass->subpassIdx = subpassIdx;

	renderSubpass->inputRef.resize(nInputRTs);
	for (UINT i = 0; i < nInputRTs; i++)
		renderSubpass->inputRef[i] = vk::AttachmentReference(static_cast<VKRenderTarget*>(ppInputRTs[i])->attachmentIdx, vk::ImageLayout::eShaderReadOnlyOptimal);

	renderSubpass->outputRef.resize(nOutputRTs);
	if (ppResolveRTs) renderSubpass->resolveRef.resize(nOutputRTs);
	for (UINT i = 0; i < nOutputRTs; i++)
	{
		renderSubpass->outputRef[i] = vk::AttachmentReference(static_cast<VKRenderTarget*>(ppOutputRTs[i])->attachmentIdx, vk::ImageLayout::eColorAttachmentOptimal);
		if (ppResolveRTs) renderSubpass->resolveRef[i] = vk::AttachmentReference(static_cast<VKRenderTarget*>(ppResolveRTs[i])->attachmentIdx, vk::ImageLayout::eColorAttachmentOptimal);
	}

	if (pDepthBuffer)
		renderSubpass->depthRef = vk::AttachmentReference(
			static_cast<const VKRenderTarget*>(pDepthBuffer)->attachmentIdx, depthWrite ?
			vk::ImageLayout::eDepthStencilAttachmentOptimal : vk::ImageLayout::eDepthStencilReadOnlyOptimal);

	renderSubpass->preservedRef.resize(nPreservedRTs);
	for (UINT i = 0; i < nPreservedRTs; i++)
		renderSubpass->preservedRef[i] = static_cast<const VKRenderTarget*>(ppPreservedRTs[i])->attachmentIdx;

	renderSubpass->desc = vk::SubpassDescription(vk::SubpassDescriptionFlags(), static_cast<vk::PipelineBindPoint>(type),
		nInputRTs, renderSubpass->inputRef.data(), nOutputRTs, renderSubpass->outputRef.data(), renderSubpass->resolveRef.data(),
		pDepthBuffer ? &renderSubpass->depthRef : nullptr, nPreservedRTs, renderSubpass->preservedRef.data());

	for (UINT i = 0; i < nDependencies; i++)
	{
		vk::SubpassDependency dependency(ppDependencies[i], renderSubpass->subpassIdx,
			vk::PipelineStageFlagBits::eColorAttachmentOutput, vk::PipelineStageFlagBits::eFragmentShader,
			vk::AccessFlagBits::eColorAttachmentWrite, vk::AccessFlagBits::eShaderRead, vk::DependencyFlagBits::eByRegion);
		renderSubpass->dependencies.push_back(dependency);
	}

	return renderSubpass;
}

void hg::Graphics::VKRenderer::DestroyRenderSubpass(RenderSubpass * pRenderSubpass)
{
}

hg::Graphics::RenderPass * hg::Graphics::VKRenderer::GetMainRenderPass()
{
	return m_mainRenderPass;
}

hg::Graphics::RenderPass * hg::Graphics::VKRenderer::CreateRenderPass(const UINT& width, const UINT& height, const float framebufferClearColor[4], const UINT & nSubpasses, RenderSubpass * const * ppSubpasses)
{
	VKRenderPass* renderPass = new VKRenderPass();
	for (UINT i = 0; i < 4; i++)
		renderPass->framebufferClearColor[i] = framebufferClearColor[i];

	renderPass->renderPassIdx = m_renderPassCount++;
	renderPass->framebufferWidth = width;
	renderPass->framebufferHeight = height;

	// Get the descriptors and dependencies for all subpasses.
	for (UINT i = 0; i < nSubpasses; i++)
	{
		const VKRenderSubpass* pVKSubpass = static_cast<const VKRenderSubpass*>(ppSubpasses[i]);
		renderPass->subpasses.push_back(pVKSubpass->desc);
		for (UINT i = 0; i < pVKSubpass->dependencies.size(); i++)
			renderPass->subpassesDependencies.push_back(pVKSubpass->dependencies[i]);
	}

	// Create the render pass.
	vk::RenderPassCreateInfo renderPassInfo(vk::RenderPassCreateFlags(),
		static_cast<uint32_t>(m_attachmentsMap[renderPass->renderPassIdx].size()), m_attachmentsMap[renderPass->renderPassIdx].data(),
		static_cast<uint32_t>(renderPass->subpasses.size()), renderPass->subpasses.data(),
		static_cast<uint32_t>(renderPass->subpassesDependencies.size()), renderPass->subpassesDependencies.data());
	ThrowIfFailed(m_device.createRenderPass(&renderPassInfo, NULL, &renderPass->handle));

	// Create the framebuffer.
	renderPass->framebuffers.resize(1);
	vk::FramebufferCreateInfo framebufferInfo(vk::FramebufferCreateFlags(), renderPass->handle,
		m_attachmentViewsMap[renderPass->renderPassIdx].size(), m_attachmentViewsMap[renderPass->renderPassIdx].data(),
		renderPass->framebufferWidth, renderPass->framebufferHeight, 1);
	ThrowIfFailed(m_device.createFramebuffer(&framebufferInfo, NULL, &renderPass->framebuffers[0]));

	return renderPass;
}

void hg::Graphics::VKRenderer::DestroyRenderPass(RenderPass * pRenderPass)
{
	VKRenderPass* pVKRenderPass = static_cast<VKRenderPass*>(pRenderPass);

	for (int i = 0; i < pVKRenderPass->framebuffers.size(); i++)
		m_device.destroyFramebuffer(pVKRenderPass->framebuffers[i], NULL);
	m_device.destroyRenderPass(static_cast<VKRenderPass*>(pRenderPass)->handle, NULL);

	delete pRenderPass;
	pRenderPass = nullptr;
}

hg::Graphics::PipelineLayout * hg::Graphics::VKRenderer::CreatePipelineLayout(const Shader* pShader)
{
	// NOTE: On glslc, the flag -Os seem to change the resources names, so it's not really reliable to find
	// the resources' binding points like this.

	VKPipelineLayout* pipelineLayout = new VKPipelineLayout();

	const VKShader* shader = static_cast<const VKShader*>(pShader);
	const ShaderData* shaderStages[SHADER_STAGES_COUNT] = { &shader->vertexStage, &shader->tesselationControlStage,
		&shader->tesselationEvaluationStage, &shader->geometryStage, &shader->fragmentStage,&shader->computeStage };

	UINT bindingMapIdx[RESOURCE_FREQUENCY_COUNT] = { 0 };
	UINT nImageSamplersPerFrequency[RESOURCE_FREQUENCY_COUNT] = { 0 };
	UINT nUniformBuffersPerFrequency[RESOURCE_FREQUENCY_COUNT] = { 0 };
	for (UINT stageIdx = 0; stageIdx < SHADER_STAGES_COUNT; stageIdx++)
	{
		if (!shaderStages[stageIdx]->source.empty())
		{
			spirv_cross::CompilerGLSL glsl(shaderStages[stageIdx]->source.data(), shaderStages[stageIdx]->source.size());
			spirv_cross::ShaderResources resources = glsl.get_shader_resources();
			for (UINT i = 0; i < resources.uniform_buffers.size(); i++)
			{
				ResourceUpdateFrequency set = static_cast<ResourceUpdateFrequency>(glsl.get_decoration(resources.uniform_buffers[i].id, spv::DecorationDescriptorSet));
				UINT binding = static_cast<ResourceUpdateFrequency>(glsl.get_decoration(resources.uniform_buffers[i].id, spv::DecorationBinding));
				size_t resourceHash = tinystl::hash_string(resources.uniform_buffers[i].name.c_str(), resources.uniform_buffers[i].name.size());

				if (pipelineLayout->bindingsMaps[set].find(resourceHash).node)
					pipelineLayout->bindings[set][resourceHash].stageFlags |= vk::ShaderStageFlagBits(1 << stageIdx);
				else
				{
					pipelineLayout->bindingsMaps[set].insert(tinystl::pair<size_t, UINT>(resourceHash, bindingMapIdx[set]++));
					pipelineLayout->bindings[set].push_back(vk::DescriptorSetLayoutBinding(binding, vk::DescriptorType::eUniformBuffer,
						1, vk::ShaderStageFlagBits(1 << stageIdx), nullptr));
					nUniformBuffersPerFrequency[set]++;
				}
			}
			for (UINT i = 0; i < resources.sampled_images.size(); i++)
			{
				ResourceUpdateFrequency set = static_cast<ResourceUpdateFrequency>(glsl.get_decoration(resources.sampled_images[i].id, spv::DecorationDescriptorSet));
				UINT binding = static_cast<ResourceUpdateFrequency>(glsl.get_decoration(resources.sampled_images[i].id, spv::DecorationBinding));
				size_t resourceHash = tinystl::hash_string(resources.sampled_images[i].name.c_str(), resources.sampled_images[i].name.size());

				if (pipelineLayout->bindingsMaps[set].find(resourceHash).node)
					pipelineLayout->bindings[set][resourceHash].stageFlags |= vk::ShaderStageFlagBits(1 << stageIdx);
				else
				{
					pipelineLayout->bindingsMaps[set].insert(tinystl::pair<size_t, UINT>(resourceHash, bindingMapIdx[set]++));
					pipelineLayout->bindings[set].push_back(vk::DescriptorSetLayoutBinding(binding, vk::DescriptorType::eCombinedImageSampler,
						1, vk::ShaderStageFlagBits(1 << stageIdx), nullptr));
					nImageSamplersPerFrequency[set]++;
				}
			}
		}
	}

	// Create the desc layout for resources of all frequencies.
	for (UINT i = 0; i < RESOURCE_FREQUENCY_COUNT; i++)
	{
		vk::DescriptorSetLayoutCreateInfo descSetLayoutInfo(vk::DescriptorSetLayoutCreateFlags(),
			static_cast<uint32_t>(pipelineLayout->bindings[i].size()), pipelineLayout->bindings[i].data());
		ThrowIfFailed(m_device.createDescriptorSetLayout(&descSetLayoutInfo, NULL, &pipelineLayout->descSetLayouts[i]));
	}

	// Create the pipeline layout.
	// TODO : Look into push constants
	vk::PipelineLayoutCreateInfo pipelineLayoutInfo(vk::PipelineLayoutCreateFlags(),
		RESOURCE_FREQUENCY_COUNT, pipelineLayout->descSetLayouts, 0, NULL);
	ThrowIfFailed(m_device.createPipelineLayout(&pipelineLayoutInfo, NULL, &pipelineLayout->handle));

	// Create descriptor pools for these descriptors, and allocate the corresponding desc sets.
	UINT nImageSamplers = nImageSamplersPerFrequency[0] + nImageSamplersPerFrequency[1] + nImageSamplersPerFrequency[2] + nImageSamplersPerFrequency[3];
	UINT nUniformBuffers = nUniformBuffersPerFrequency[0] + nUniformBuffersPerFrequency[1] + nUniformBuffersPerFrequency[2] + nUniformBuffersPerFrequency[3];
	UINT nDescriptorSets = nImageSamplers + nUniformBuffers /* + others*/;

	tinystl::vector<vk::DescriptorPoolSize> poolSizes(2);
	poolSizes[0] = vk::DescriptorPoolSize(vk::DescriptorType::eCombinedImageSampler, nImageSamplers);
	poolSizes[1] = vk::DescriptorPoolSize(vk::DescriptorType::eUniformBuffer, nUniformBuffers);

	vk::DescriptorPoolCreateInfo descPoolInfo(vk::DescriptorPoolCreateFlags(), nDescriptorSets, static_cast<uint32_t>(poolSizes.size()), poolSizes.data());
	ThrowIfFailed(m_device.createDescriptorPool(&descPoolInfo, NULL, &pipelineLayout->descPool));

	for (UINT i = 0; i < RESOURCE_FREQUENCY_COUNT; i++)
	{
		UINT nDescriptors = nImageSamplersPerFrequency[i] + nUniformBuffersPerFrequency[i] /* + others[i]*/;
		if (nDescriptors > 0)
		{
			vk::DescriptorSetAllocateInfo descSetAlloc(pipelineLayout->descPool, nDescriptors, &pipelineLayout->descSetLayouts[i]);
			ThrowIfFailed(m_device.allocateDescriptorSets(&descSetAlloc, &pipelineLayout->descSets[i]));
		}
	}

	return pipelineLayout;
}

void hg::Graphics::VKRenderer::DestroyPipelineLayout(PipelineLayout * pPipelineLayout)
{
	m_device.destroyDescriptorPool(static_cast<VKPipelineLayout*>(pPipelineLayout)->descPool, NULL);
	m_device.destroyPipelineLayout(static_cast<VKPipelineLayout*>(pPipelineLayout)->handle, NULL);

	for (UINT i = 0; i < RESOURCE_FREQUENCY_COUNT; i++)
		m_device.destroyDescriptorSetLayout(static_cast<VKPipelineLayout*>(pPipelineLayout)->descSetLayouts[i], NULL);

	delete pPipelineLayout;
	pPipelineLayout = nullptr;
}

hg::Graphics::PipelineState * hg::Graphics::VKRenderer::CreatePipelineState(const Shader * pShader, const PipelineLayout * pPipelineLayout, const RenderPass * pRenderPass, const UINT & subpassIdx, const CullMode & cullMode)
{
	VKPipelineState* pipelineState = new VKPipelineState();

	// Shader stages configuration
	const VKShader* pVKShader = static_cast<const VKShader*>(pShader);
	tinystl::vector<vk::PipelineShaderStageCreateInfo> shaderStagesInfo;
	if (pVKShader->vertexStage.module)
		shaderStagesInfo.push_back(vk::PipelineShaderStageCreateInfo(vk::PipelineShaderStageCreateFlags(),
			vk::ShaderStageFlagBits::eVertex, pVKShader->vertexStage.module, "main", NULL));
	if (pVKShader->tesselationControlStage.module)
		shaderStagesInfo.push_back(vk::PipelineShaderStageCreateInfo(vk::PipelineShaderStageCreateFlags(),
			vk::ShaderStageFlagBits::eTessellationControl, pVKShader->tesselationControlStage.module, "main", NULL));
	if (pVKShader->tesselationEvaluationStage.module)
		shaderStagesInfo.push_back(vk::PipelineShaderStageCreateInfo(vk::PipelineShaderStageCreateFlags(),
			vk::ShaderStageFlagBits::eTessellationEvaluation, pVKShader->tesselationEvaluationStage.module, "main", NULL));
	if (pVKShader->geometryStage.module)
		shaderStagesInfo.push_back(vk::PipelineShaderStageCreateInfo(vk::PipelineShaderStageCreateFlags(),
			vk::ShaderStageFlagBits::eGeometry, pVKShader->geometryStage.module, "main", NULL));
	if (pVKShader->fragmentStage.module)
		shaderStagesInfo.push_back(vk::PipelineShaderStageCreateInfo(vk::PipelineShaderStageCreateFlags(),
			vk::ShaderStageFlagBits::eFragment, pVKShader->fragmentStage.module, "main", NULL));
	if (pVKShader->computeStage.module)
		shaderStagesInfo.push_back(vk::PipelineShaderStageCreateInfo(vk::PipelineShaderStageCreateFlags(),
			vk::ShaderStageFlagBits::eCompute, pVKShader->computeStage.module, "main", NULL));

	// Dynamic State Configuration
	vk::DynamicState dynamicStateEnabled[VK_DYNAMIC_STATE_RANGE_SIZE] = {};
	vk::PipelineDynamicStateCreateInfo dynamicStateInfo(vk::PipelineDynamicStateCreateFlags(), 0, dynamicStateEnabled);

	// Vertex Input State Configuration
	// TODO : Read this info properly from vertex data.
	vk::VertexInputBindingDescription vertexBinding(0, 20, vk::VertexInputRate::eVertex);
	vk::VertexInputAttributeDescription vertexAttribs[2] = {
		vk::VertexInputAttributeDescription(0, 0, vk::Format::eR32G32B32Sfloat, 0),
		vk::VertexInputAttributeDescription(1, 0, vk::Format::eR32G32Sfloat, 12) };
	vk::PipelineVertexInputStateCreateInfo vertexInputStateInfo(vk::PipelineVertexInputStateCreateFlags(), 1,
		&vertexBinding, 2, vertexAttribs);

	// Input Assembly State Configuration
	// TODO : Read this info properly from vertex data.
	vk::PipelineInputAssemblyStateCreateInfo inputAssemblyStateInfo(vk::PipelineInputAssemblyStateCreateFlags(),
		vk::PrimitiveTopology::eTriangleList, VK_FALSE);

	// Rasterization State Configuration
	vk::PipelineRasterizationStateCreateInfo rasterizationStateInfo(vk::PipelineRasterizationStateCreateFlags(),
		VK_TRUE, VK_FALSE, vk::PolygonMode::eFill, vk::CullModeFlagBits(cullMode), vk::FrontFace::eClockwise, VK_FALSE,
		0.0f, 0.0f, 0.0f, 1.0f);

	// Color Blend State Configuration
	vk::PipelineColorBlendAttachmentState attachmentState[1] = {
		vk::PipelineColorBlendAttachmentState(VK_FALSE,
		vk::BlendFactor::eZero, vk::BlendFactor::eZero, vk::BlendOp::eAdd,
		vk::BlendFactor::eZero, vk::BlendFactor::eZero, vk::BlendOp::eAdd,
		vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA) };
	vk::PipelineColorBlendStateCreateInfo colorBlendStateInfo(vk::PipelineColorBlendStateCreateFlags(), VK_FALSE,
		vk::LogicOp::eNoOp, 1, attachmentState, { 1.0f, 1.0f, 1.0f, 1.0f });

	// Viewport State Configuration
	// This should be always dynamic, due to window resizing.
	dynamicStateEnabled[dynamicStateInfo.dynamicStateCount++] = vk::DynamicState::eViewport;
	dynamicStateEnabled[dynamicStateInfo.dynamicStateCount++] = vk::DynamicState::eScissor;
	vk::PipelineViewportStateCreateInfo vieportStateInfo(vk::PipelineViewportStateCreateFlags(), 1, NULL, 1, NULL);

	// Depth Stencil State Configuration
	vk::PipelineDepthStencilStateCreateInfo depthStencilStateInfo(vk::PipelineDepthStencilStateCreateFlags(), VK_TRUE,
		VK_TRUE, vk::CompareOp::eGreaterOrEqual, VK_FALSE, VK_FALSE,
		vk::StencilOpState(vk::StencilOp::eKeep, vk::StencilOp::eKeep,
			vk::StencilOp::eKeep, vk::CompareOp::eAlways, 0, 0, 0),
		vk::StencilOpState(vk::StencilOp::eKeep, vk::StencilOp::eKeep,
			vk::StencilOp::eKeep, vk::CompareOp::eAlways, 0, 0, 0), 0, 0);

	// Multisample State Configuration
	vk::PipelineMultisampleStateCreateInfo multiSampleStateInfo(vk::PipelineMultisampleStateCreateFlags(),
		vk::SampleCountFlagBits::e1, VK_FALSE, 0.0f, NULL, VK_FALSE, VK_FALSE);

	// Pipeline Creation
	vk::GraphicsPipelineCreateInfo pipelineInfo(vk::PipelineCreateFlags(), shaderStagesInfo.size(), shaderStagesInfo.data(),
		&vertexInputStateInfo, &inputAssemblyStateInfo, NULL, &vieportStateInfo, &rasterizationStateInfo,
		&multiSampleStateInfo, &depthStencilStateInfo, &colorBlendStateInfo, &dynamicStateInfo,
		static_cast<const VKPipelineLayout*>(pPipelineLayout)->handle,
		static_cast<const VKRenderPass*>(pRenderPass)->handle, subpassIdx, nullptr, 0u);
	ThrowIfFailed(m_device.createGraphicsPipelines(nullptr, 1, &pipelineInfo, NULL, &pipelineState->handle));

	return pipelineState;
}

void hg::Graphics::VKRenderer::DestroyPipelineState(PipelineState * pPipelineState)
{
	m_device.destroyPipeline(static_cast<VKPipelineState*>(pPipelineState)->handle, NULL);
	delete pPipelineState;
	pPipelineState = nullptr;
}

hg::Graphics::CommandRecorder * hg::Graphics::VKRenderer::CreateCommandRecorder(const CommandRecorderType& type)
{
	VKCommandRecorder * cmdRecorder = new VKCommandRecorder();

	// Create the command list.
	vk::CommandBufferAllocateInfo cmdListInfo(m_commandPool, vk::CommandBufferLevel::ePrimary, 1);
	ThrowIfFailed(m_device.allocateCommandBuffers(&cmdListInfo, &cmdRecorder->cmdBuffer));

	return cmdRecorder;
}

void hg::Graphics::VKRenderer::DestroyCommandRecorder(CommandRecorder * pCommandRecorder)
{
	m_device.freeCommandBuffers(m_commandPool, static_cast<VKCommandRecorder*>(pCommandRecorder)->cmdBuffer);
	delete pCommandRecorder;
	pCommandRecorder = nullptr;
}

hg::Graphics::CommandBundle * hg::Graphics::VKRenderer::CreateCommandBundle(const RenderPass* pRenderPass, const UINT& subpassIdx)
{
	VKCommandBundle * cmdBundle = new VKCommandBundle();

	// Create the command bundle.
	vk::CommandBufferAllocateInfo cmdBundleInfo(m_bundlePool, vk::CommandBufferLevel::eSecondary, 1);
	ThrowIfFailed(m_device.allocateCommandBuffers(&cmdBundleInfo, &cmdBundle->cmdBuffer));

	// Command buffer must be created in the recording state.
	vk::CommandBufferInheritanceInfo inheritanceInfo(static_cast<const VKRenderPass *>(pRenderPass)->handle, subpassIdx,
		nullptr, VK_FALSE, vk::QueryControlFlags(), vk::QueryPipelineStatisticFlags());
	vk::CommandBufferBeginInfo beginInfo(vk::CommandBufferUsageFlagBits::eSimultaneousUse |
		vk::CommandBufferUsageFlagBits::eRenderPassContinue, &inheritanceInfo);
	cmdBundle->cmdBuffer.begin(beginInfo);

	return cmdBundle;
}

void hg::Graphics::VKRenderer::DestroyCommandBundle(CommandBundle * pCommandBundle)
{
	m_device.freeCommandBuffers(m_bundlePool, static_cast<VKCommandBundle*>(pCommandBundle)->cmdBuffer);
	delete pCommandBundle;
	pCommandBundle = nullptr;
}

void hg::Graphics::VKRenderer::BindTexture(const tinystl::string & name, Texture * pTexture, Sampler * pSampler, PipelineLayout * pPipelineLayout)
{
	VKTexture* pVKTexture = static_cast<VKTexture*>(pTexture);
	VKSampler* pVKSampler = static_cast<VKSampler*>(pSampler);
	VKPipelineLayout* pVKPipelineLayout = static_cast<VKPipelineLayout*>(pPipelineLayout);

	size_t resourceHash = tinystl::hash_string(name.c_str(), name.size());

	bool resourceFound = false;
	UINT frequencyIdx = 0;
	for (frequencyIdx = 0; frequencyIdx < 4; frequencyIdx++)
	{
		resourceFound = pVKPipelineLayout->bindingsMaps[frequencyIdx].find(resourceHash).node;
		if (resourceFound) break;
	}
	if (!resourceFound)
		throw std::exception();

	vk::DescriptorSetLayoutBinding descBinding = pVKPipelineLayout->bindings[frequencyIdx][pVKPipelineLayout->bindingsMaps[frequencyIdx].find(resourceHash).node->second];
	vk::DescriptorImageInfo imageDesc(pVKSampler->handle, pVKTexture->view, vk::ImageLayout::eShaderReadOnlyOptimal);
	vk::WriteDescriptorSet imageUpdate(pVKPipelineLayout->descSets[frequencyIdx], descBinding.binding, 0, 1, descBinding.descriptorType,
		&imageDesc, nullptr, nullptr);
	m_device.updateDescriptorSets(1, &imageUpdate, 0, nullptr);
}

void hg::Graphics::VKRenderer::BindTexture(const tinystl::string & name, RenderTarget * pTexture, Sampler * pSampler, PipelineLayout * pPipelineLayout)
{
	VKRenderTarget* pVKTexture = static_cast<VKRenderTarget*>(pTexture);
	VKSampler* pVKSampler = static_cast<VKSampler*>(pSampler);
	VKPipelineLayout* pVKPipelineLayout = static_cast<VKPipelineLayout*>(pPipelineLayout);

	size_t resourceHash = tinystl::hash_string(name.c_str(), name.size());

	bool resourceFound = false;
	UINT frequencyIdx = 0;
	for (frequencyIdx = 0; frequencyIdx < 4; frequencyIdx++)
	{
		resourceFound = pVKPipelineLayout->bindingsMaps[frequencyIdx].find(resourceHash).node;
		if (resourceFound) break;
	}
	if (!resourceFound)
		throw std::exception();

	vk::DescriptorSetLayoutBinding descBinding = pVKPipelineLayout->bindings[frequencyIdx][pVKPipelineLayout->bindingsMaps[frequencyIdx].find(resourceHash).node->second];
	vk::DescriptorImageInfo imageDesc(pVKSampler->handle, pVKTexture->view, vk::ImageLayout::eShaderReadOnlyOptimal);
	vk::WriteDescriptorSet imageUpdate(pVKPipelineLayout->descSets[frequencyIdx], descBinding.binding, 0, 1, descBinding.descriptorType,
		&imageDesc, nullptr, nullptr);
	m_device.updateDescriptorSets(1, &imageUpdate, 0, nullptr);
}

void hg::Graphics::VKRenderer::BindSampler(const tinystl::string & name, Sampler * pSampler, PipelineLayout * pPipelineLayout)
{
	VKSampler* pVKSampler = static_cast<VKSampler*>(pSampler);
	VKPipelineLayout* pVKPipelineLayout = static_cast<VKPipelineLayout*>(pPipelineLayout);

	size_t resourceHash = tinystl::hash_string(name.c_str(), name.size());

	bool resourceFound = false;
	UINT frequencyIdx = 0;
	for (frequencyIdx = 0; frequencyIdx < 4; frequencyIdx++)
	{
		resourceFound = pVKPipelineLayout->bindingsMaps[frequencyIdx].find(resourceHash).node;
		if (resourceFound) break;
	}
	if (!resourceFound)
		throw std::exception();

	vk::DescriptorSetLayoutBinding descBinding = pVKPipelineLayout->bindings[frequencyIdx][pVKPipelineLayout->bindingsMaps[frequencyIdx].find(resourceHash).node->second];
	vk::DescriptorImageInfo samplerDesc(pVKSampler->handle, vk::ImageView(), vk::ImageLayout::eUndefined);
	vk::WriteDescriptorSet samplerUpdate(pVKPipelineLayout->descSets[frequencyIdx], descBinding.binding, 0, 1, descBinding.descriptorType,
		&samplerDesc, nullptr, nullptr);
	m_device.updateDescriptorSets(1, &samplerUpdate, 0, nullptr);
}

void hg::Graphics::VKRenderer::BindBuffer(const tinystl::string & name, Buffer * pBuffer, PipelineLayout * pPipelineLayout)
{
	VKBuffer* pVKBuffer = static_cast<VKBuffer*>(pBuffer);
	VKPipelineLayout* pVKPipelineLayout = static_cast<VKPipelineLayout*>(pPipelineLayout);

	size_t resourceHash = tinystl::hash_string(name.c_str(), name.size());

	bool resourceFound = false;
	UINT frequencyIdx = 0;
	for (frequencyIdx = 0; frequencyIdx < 4; frequencyIdx++)
	{
		resourceFound = pVKPipelineLayout->bindingsMaps[frequencyIdx].find(resourceHash).node;
		if (resourceFound) break;
	}
	if (!resourceFound)
		throw std::exception();

	vk::DescriptorSetLayoutBinding descBinding = pVKPipelineLayout->bindings[frequencyIdx][pVKPipelineLayout->bindingsMaps[frequencyIdx].find(resourceHash).node->second];
	vk::DescriptorBufferInfo bufferDesc(pVKBuffer->handle, 0, pVKBuffer->size);
	vk::WriteDescriptorSet bufferUpdate(pVKPipelineLayout->descSets[frequencyIdx], descBinding.binding, 0, 1, descBinding.descriptorType,
		nullptr, &bufferDesc, nullptr);
	m_device.updateDescriptorSets(1, &bufferUpdate, 0, nullptr);
}

void hg::Graphics::VKRenderer::ExecuteCommands(unsigned int nCmdRecorders, CommandRecorder* const *ppCommandRecorders)
{
	tinystl::vector<vk::CommandBuffer> cmdBufferArray(nCmdRecorders);
	tinystl::vector<vk::SubmitInfo> submitInfoArray(nCmdRecorders);

	vk::PipelineStageFlags pipelineStageFlags = vk::PipelineStageFlagBits::eBottomOfPipe;
	for (UINT i = 0; i < nCmdRecorders; i++)
	{
		cmdBufferArray[i] = static_cast<const VKCommandRecorder *>(ppCommandRecorders[i])->cmdBuffer;
		submitInfoArray[i] = vk::SubmitInfo(1u, &m_semaphore, &pipelineStageFlags, 1u, &cmdBufferArray[i], 0u, NULL);
	}

	ThrowIfFailed(m_mainQueue.submit(nCmdRecorders, submitInfoArray.data(), m_fence));
}

void hg::Graphics::VKRenderer::FinishFrame()
{
	vk::Result res;
	UINT64 timeout = 100000000;
	do {
		res = m_device.waitForFences(1, &m_fence, VK_TRUE, timeout);
	} while (res == vk::Result::eTimeout);
	m_device.resetFences(1, &m_fence);

	ThrowIfFailed(m_device.acquireNextImageKHR(m_swapchain, UINT64_MAX, m_semaphore, nullptr, &m_mainFramebufferIdx));

	m_device.resetCommandPool(m_commandPool, vk::CommandPoolResetFlagBits());
}

void hg::Graphics::VKRenderer::Present()
{
	vk::PresentInfoKHR presentInfo(0, NULL, 1, &m_swapchain, &m_mainFramebufferIdx, NULL);
	ThrowIfFailed(m_mainQueue.presentKHR(&presentInfo));
}

void hg::Graphics::VKRenderer::ResizeMainFramebuffer()
{
	// Wait until the device is idle.
	m_device.waitIdle();

	// Destroy the current framebuffer resources and swapchain.
	DestroyRenderPass(m_mainRenderPass);
	m_device.destroyImageView(m_mainFramebufferViews[0], NULL);
	m_device.destroyImageView(m_mainFramebufferViews[1], NULL);
	m_device.destroySwapchainKHR(m_swapchain, NULL);
	
	// Recreate the synchronization primitives.
	m_device.destroyFence(m_fence, NULL);
	m_device.destroySemaphore(m_semaphore, NULL);
	ThrowIfFailed(m_device.createSemaphore(&vk::SemaphoreCreateInfo(), nullptr, &m_semaphore));
	ThrowIfFailed(m_device.createFence(&vk::FenceCreateInfo(), nullptr, &m_fence));

	// Reinitialize the swapchain.
	initSwapchain();

	// Recreate the main render pass.
	initMainRenderPass();

	// Reset the bundle cmdPool.
	m_device.resetCommandPool(m_bundlePool, vk::CommandPoolResetFlagBits());
}
