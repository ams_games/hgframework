#pragma once

#include "base\src\HGGraphics.h"

// NOTE : Maybe I should change this to HGPlatform?
#if defined(_WIN32)
#define VK_USE_PLATFORM_WIN32_KHR
#endif

#include <vulkan\vulkan.hpp>

#include "HGVKShader.h"
#include "HGVKResources.h"
#include "HGVKPipeline.h"
#include "HGVKCommands.h"

namespace hg
{
	namespace Graphics
	{
		// ENUMS

		enum TextureFormat
		{
			DEFAULT = VK_FORMAT_UNDEFINED,
			R8G8B8A8_UNORM = VK_FORMAT_R8G8B8A8_UNORM,
			R8G8B8A8_SRGB = VK_FORMAT_R8G8B8A8_SRGB,
			D32 = VK_FORMAT_D32_SFLOAT
		};

		// STRUCTS

		struct Rect : public vk::Rect2D
		{
			Rect() {};
			Rect(const LONG& l, const LONG& t, const LONG& r, const LONG& b)
			{
				offset.x = l;
				offset.y = t;
				extent.width = r - l;
				extent.height = b - t;
			}
		};
		struct Viewport : public vk::Viewport
		{
			Viewport() {};
			Viewport(const float& originX, const float& originY, const float& width, const float& height, const float& minDepth, const float& maxDepth)
			{
				x = originX;
				y = originY;
				this->width = width;
				this->height = height;
				this->minDepth = minDepth;
				this->maxDepth = maxDepth;
			}
		};

		// INTERFACES

		class VKShader;
		class VKBuffer;
		class VKTexture;
		class VKRenderTarget;
		class VKRenderSubpass;
		class VKRenderPass;
		class VKPipelineLayout;
		class VKPipelineState;
		class VKCommandRecorder;
		class VKCommandBundle;
		class VKRenderer;
	}
}