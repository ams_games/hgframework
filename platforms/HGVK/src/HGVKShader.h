#pragma once

#include "base\src\HGShader.h"
#include "HGVK.h"

namespace hg
{
	namespace Graphics
	{
		struct ShaderData
		{
			tinystl::vector<UINT> source;
			vk::ShaderModule module;
		};

		class VKShader : public Shader
		{
		public:
			ShaderData vertexStage;
			ShaderData tesselationControlStage;
			ShaderData tesselationEvaluationStage;
			ShaderData geometryStage;
			ShaderData fragmentStage;
			ShaderData computeStage;
		};
	}
}