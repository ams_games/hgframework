#include "HGVKCommands.h"

#include "HGVKUtils.h"
#include "HGVKResources.h"
#include "HGVKPipeline.h"

///////////// COMMMAND RECORDER /////////////

void hg::Graphics::VKCommandRecorder::Reset()
{
	vk::CommandBufferBeginInfo beginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit, NULL);
	ThrowIfFailed(cmdBuffer.begin(&beginInfo));
}

void hg::Graphics::VKCommandRecorder::Close()
{
	cmdBuffer.end();
}

void hg::Graphics::VKCommandRecorder::ExecuteCommandBundles(unsigned int nCmdBundles, CommandBundle * const * ppCommandBundles)
{
	tinystl::vector<vk::CommandBuffer> cmdBundleArray(nCmdBundles);

	for (UINT i = 0; i < nCmdBundles; i++)
		cmdBundleArray[i] = static_cast<const VKCommandBundle*>(ppCommandBundles[i])->cmdBuffer;

	cmdBuffer.executeCommands(nCmdBundles, cmdBundleArray.data());
}

void hg::Graphics::VKCommandRecorder::BeginRenderPass(const RenderPass * pRenderPass)
{
	const VKRenderPass* vkRenderPass = static_cast<const VKRenderPass *>(pRenderPass);

	vk::ClearValue clearValues[2];
	clearValues[0].color.float32[0] = vkRenderPass->framebufferClearColor[0];
	clearValues[0].color.float32[1] = vkRenderPass->framebufferClearColor[1];
	clearValues[0].color.float32[2] = vkRenderPass->framebufferClearColor[2];
	clearValues[0].color.float32[3] = vkRenderPass->framebufferClearColor[3];
	clearValues[1].depthStencil = 0.0f;

	vk::RenderPassBeginInfo renderPassBeginInfo(vkRenderPass->handle,
		static_cast<const VKRenderPass *>(pRenderPass)->framebuffers[0],
		vk::Rect2D(vk::Offset2D(0, 0), vk::Extent2D(static_cast<const VKRenderPass *>(pRenderPass)->framebufferWidth,
			static_cast<const VKRenderPass *>(pRenderPass)->framebufferHeight)), 2, clearValues);

	cmdBuffer.beginRenderPass(&renderPassBeginInfo, vk::SubpassContents::eInline);
}

void hg::Graphics::VKCommandRecorder::BeginMainRenderPass(const RenderPass * pRenderPass, const UINT currentFramebufferIdx)
{
	const VKRenderPass* vkRenderPass = static_cast<const VKRenderPass *>(pRenderPass);

	vk::ClearValue clearValue;
	clearValue.color.float32[0] = vkRenderPass->framebufferClearColor[0];
	clearValue.color.float32[1] = vkRenderPass->framebufferClearColor[1];
	clearValue.color.float32[2] = vkRenderPass->framebufferClearColor[2];
	clearValue.color.float32[3] = vkRenderPass->framebufferClearColor[3];

	vk::RenderPassBeginInfo renderPassBeginInfo(vkRenderPass->handle,
		static_cast<const VKRenderPass *>(pRenderPass)->framebuffers[currentFramebufferIdx],
		vk::Rect2D(vk::Offset2D(0, 0), vk::Extent2D(static_cast<const VKRenderPass *>(pRenderPass)->framebufferWidth,
		static_cast<const VKRenderPass *>(pRenderPass)->framebufferHeight)), 1, &clearValue);

	cmdBuffer.beginRenderPass(&renderPassBeginInfo, vk::SubpassContents::eInline);
}

void hg::Graphics::VKCommandRecorder::AdvanceSubpass()
{
	cmdBuffer.nextSubpass(vk::SubpassContents::eInline);
}

void hg::Graphics::VKCommandRecorder::EndRenderPass()
{
	cmdBuffer.endRenderPass();
}

void hg::Graphics::VKCommandRecorder::SetPipelineLayout(const PipelineLayout * pPipelineLayout)
{
	for (UINT i = 0; i < RESOURCE_FREQUENCY_COUNT; i++)
	{
		if(static_cast<const VKPipelineLayout*>(pPipelineLayout)->descSets[i]) 
			cmdBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics,
				static_cast<const VKPipelineLayout*>(pPipelineLayout)->handle, i, 1,
				&static_cast<const VKPipelineLayout*>(pPipelineLayout)->descSets[i], 0, nullptr);
	}
}

void hg::Graphics::VKCommandRecorder::SetPipelineState(const PipelineState * pPipelineState)
{
	cmdBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, static_cast<const VKPipelineState*>(pPipelineState)->handle);
}

void hg::Graphics::VKCommandRecorder::SetPrimitiveTopology(PrimitiveTopology primitiveTopology)
{

}

void hg::Graphics::VKCommandRecorder::SetVertexBuffers(UINT start, UINT nViews, Buffer * const * ppBuffers)
{
	tinystl::vector<vk::Buffer> vertexbufferArray(nViews);

	for (UINT i = 0; i < nViews; i++)
		vertexbufferArray[i] = static_cast<const VKBuffer*>(ppBuffers[i])->handle;

	const vk::DeviceSize offsets[1] = { 0 };
	cmdBuffer.bindVertexBuffers(start, nViews, vertexbufferArray.data(), offsets);
}

void hg::Graphics::VKCommandRecorder::SetIndexBuffer(Buffer * pBuffer)
{
	cmdBuffer.bindIndexBuffer(static_cast<VKBuffer*>(pBuffer)->handle, 0, vk::IndexType::eUint32);
}

void hg::Graphics::VKCommandRecorder::SetScissorRects(UINT nRects, const Rect * pRects)
{
	cmdBuffer.setScissor(0, nRects, pRects);
}

void hg::Graphics::VKCommandRecorder::SetViewports(UINT nViewports, const Viewport * pViewports)
{
	cmdBuffer.setViewport(0, nViewports, pViewports);
}

void hg::Graphics::VKCommandRecorder::SetBlendFactor(const FLOAT blendFactors[4])
{

}

void hg::Graphics::VKCommandRecorder::SetStencilRefValue(UINT stencilRefValue)
{

}

void hg::Graphics::VKCommandRecorder::Draw(const UINT & nVertices, const UINT & nInstances, const UINT & vertexStart, const UINT & instanceStart)
{
	cmdBuffer.draw(nVertices, nInstances, vertexStart, instanceStart);
}

void hg::Graphics::VKCommandRecorder::DrawIndexed(const UINT & nIndices, const UINT & nInstances, const UINT & indexStart, const UINT & vertexOffset, const UINT & instanceStart)
{
	cmdBuffer.drawIndexed(nIndices, nInstances, indexStart, vertexOffset, instanceStart);
}

///////////// COMMMAND BUNDLE /////////////

void hg::Graphics::VKCommandBundle::Reset()
{
	vk::CommandBufferBeginInfo beginInfo(vk::CommandBufferUsageFlagBits::eSimultaneousUse | vk::CommandBufferUsageFlagBits::eRenderPassContinue, NULL);
	ThrowIfFailed(cmdBuffer.begin(&beginInfo));
}

void hg::Graphics::VKCommandBundle::Close()
{
	cmdBuffer.end();
}

void hg::Graphics::VKCommandBundle::SetPipelineLayout(const PipelineLayout * pPipelineLayout)
{
	cmdBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics,
		static_cast<const VKPipelineLayout*>(pPipelineLayout)->handle, 0, 4,
		&static_cast<const VKPipelineLayout*>(pPipelineLayout)->descSets[0], 0, nullptr);
}

void hg::Graphics::VKCommandBundle::SetPipelineState(const PipelineState * pPipelineState)
{
	cmdBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, static_cast<const VKPipelineState*>(pPipelineState)->handle);
}

void hg::Graphics::VKCommandBundle::SetPrimitiveTopology(PrimitiveTopology primitiveTopology)
{
}

void hg::Graphics::VKCommandBundle::SetVertexBuffers(UINT start, UINT nViews, Buffer * const * ppBuffers)
{
	tinystl::vector<vk::Buffer> vertexbufferArray(nViews);

	for (UINT i = 0; i < nViews; i++)
		vertexbufferArray[i] = static_cast<const VKBuffer*>(ppBuffers[i])->handle;

	const vk::DeviceSize offsets[1] = { 0 };
	cmdBuffer.bindVertexBuffers(0, nViews, vertexbufferArray.data(), offsets);
}

void hg::Graphics::VKCommandBundle::SetBlendFactor(const FLOAT blendFactors[4])
{
}

void hg::Graphics::VKCommandBundle::SetStencilRefValue(UINT stencilRefValue)
{
}

void hg::Graphics::VKCommandBundle::DrawInstanced(UINT verticesPerInstance, UINT nInstances, UINT vertexStart, UINT instanceStart)
{
	cmdBuffer.draw(verticesPerInstance, nInstances, vertexStart, instanceStart);
}
