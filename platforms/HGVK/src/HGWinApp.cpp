#include "HGWinApp.h"

hg::WinApp::WinApp(const UINT & width, const UINT & height, const std::wstring & name) : BaseApp(width, height, name)
{

}

void hg::WinApp::ParseCmdLineArgs(const char * cmds)
{
	BaseApp::ParseCmdLineArgs(cmds);

	// TODO : Add specific Windows debugging options / modes.
}

void hg::WinApp::GetWindowCenter(UINT & centerX, UINT & centerY)
{
	RECT windowRect;
	GetWindowRect(m_hWindow, &windowRect);
	centerX = windowRect.left + (windowRect.right - windowRect.left) / 2;
	centerY = windowRect.top + (windowRect.bottom - windowRect.top) / 2;
}

void hg::WinApp::TrapCursor()
{
	m_cursorVisible = false;
	ShowCursor(m_cursorVisible);

	RECT windowRect;
	GetWindowRect(m_hWindow, &windowRect);
	ClipCursor(&windowRect);

	SetTimer(m_hWindow, 0, 16,(TIMERPROC)NULL);
}

void hg::WinApp::FreeCursor()
{
	m_cursorVisible = true;
	ShowCursor(m_cursorVisible);
	ClipCursor(NULL);
	KillTimer(m_hWindow, 0);
}

bool hg::WinApp::IsKeyPressed(const UINT8 & key)
{
	return GetAsyncKeyState(key);
}

bool hg::WinApp::initPlatform()
{
	m_renderer = (static_cast<Graphics::Renderer*>(new Graphics::VKRenderer()));
	return true;
}

bool hg::WinApp::destroyPlatform()
{
	delete static_cast<Graphics::VKRenderer*>(m_renderer);
	m_renderer = NULL;
	return true;
}
