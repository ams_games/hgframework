#pragma once

#include "base\src\HGPipeline.h"
#include "HGVK.h"

namespace hg
{
	namespace Graphics
	{
		class VKPipelineLayout : public PipelineLayout
		{
		public:
			vk::PipelineLayout handle;
			vk::DescriptorPool descPool;

			vk::DescriptorSet descSets[RESOURCE_FREQUENCY_COUNT];
			vk::DescriptorSetLayout descSetLayouts[RESOURCE_FREQUENCY_COUNT];

			tinystl::unordered_map<size_t, UINT> bindingsMaps[RESOURCE_FREQUENCY_COUNT];
			tinystl::vector<vk::DescriptorSetLayoutBinding> bindings[RESOURCE_FREQUENCY_COUNT];
		};

		class VKPipelineState : public PipelineState
		{
		public:
			vk::Pipeline handle;
		};
	}
}

