#pragma once

#include "base\src\HGPlatform.h"
#include "base\src\HGContainers.h"
#include <vulkan\vulkan.hpp>

inline void ThrowIfFailed(const vk::Result& result)
{
	if (FAILED(result))
	{
		throw std::exception();
	}
}

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
	VkDebugReportFlagsEXT flags,
	VkDebugReportObjectTypeEXT objType,
	uint64_t obj,
	size_t location,
	int32_t code,
	const char* layerPrefix,
	const char* msg,
	void* userData) {

	wchar_t debugMsg[1024];
	std::string debugString;

	if (flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT) debugString += "VK INFO:	";
	else if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT) debugString += "VK WARNING: ";
	else if (flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT) debugString += "VK PERF:	";
	else if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT) debugString += "VK ERROR:	";
	else if (flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT) debugString += "VK DEBUG:	";

	debugString = debugString + '(' + layerPrefix + ") " + msg + '\n';

	size_t nCharsConverted;
	mbstowcs_s(&nCharsConverted, debugMsg, debugString.data(), 1024);

	OutputDebugString(debugMsg);

	return VK_FALSE;
}

static bool ValidateVKExtension(const char * extensionName, const tinystl::vector<vk::ExtensionProperties> * pSupportedExtensions)
{
	for (auto it = pSupportedExtensions->begin(); it != pSupportedExtensions->end(); it++)
	{
		if (strcmp(it->extensionName, extensionName) == 0) return true;
	}

	return false;
}

static bool ValidateVKLayer(const char * layerName, const tinystl::vector<vk::LayerProperties> * pSupportedLayers)
{
	for (auto it = pSupportedLayers->begin(); it != pSupportedLayers->end(); it++)
	{
		if (strcmp(it->layerName, layerName) == 0) return true;
	}

	return false;
}

static uint32_t GetMemoryTypeFromProperties(uint32_t typeBits, const vk::PhysicalDeviceMemoryProperties& gpuMemProperties, const vk::MemoryPropertyFlags& requirementsMask) {
	// Search memtypes to find first index with those properties
	for (uint32_t i = 0; i < gpuMemProperties.memoryTypeCount; i++) {
		if ((typeBits & 1) == 1) {
			// Type is available, does it match user properties?
			if ((gpuMemProperties.memoryTypes[i].propertyFlags & requirementsMask) == requirementsMask)
				return i;
		}
		typeBits >>= 1;
	}
	// No memory types matched, return failure
	return -1;
}