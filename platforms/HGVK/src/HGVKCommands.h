#pragma once

#include "base\src\HGCommands.h"
#include "HGVK.h"

namespace hg
{
	namespace Graphics
	{
		class VKCommandRecorder : public CommandRecorder
		{
		public:
			virtual void Reset() override;
			virtual void Close() override;

			virtual void ExecuteCommandBundles(unsigned int nCmdBundles, CommandBundle *const *ppCommandBundles) override;

			virtual void BeginRenderPass(const RenderPass * pRenderPass) override;
			virtual void BeginMainRenderPass(const RenderPass * pRenderPass, const UINT currentFramebufferIdx) override;
			virtual void AdvanceSubpass() override;
			virtual void EndRenderPass() override;

			virtual void SetPipelineLayout(const PipelineLayout * pPipelineLayout) override;
			virtual void SetPipelineState(const PipelineState * pPipelineState) override;

			virtual void SetPrimitiveTopology(PrimitiveTopology primitiveTopology) override;
			virtual void SetVertexBuffers(UINT start, UINT nViews, Buffer * const * ppBuffers) override;
			virtual void SetIndexBuffer(Buffer* pBuffer) override;

			virtual void SetScissorRects(UINT nRects, const Rect * pRects) override;
			virtual void SetViewports(UINT nViewports, const Viewport * pViewports) override;

			virtual void SetBlendFactor(const FLOAT blendFactors[4]) override;
			virtual void SetStencilRefValue(UINT stencilRefValue) override;

			virtual void Draw(const UINT& nVertices, const UINT& nInstances, const UINT& vertexStart, const UINT& instanceStart) override;
			virtual void DrawIndexed(const UINT& nIndices, const UINT& nInstances, const UINT& indexStart, const UINT& vertexOffset, const UINT& instanceStart) override;

			vk::CommandBuffer cmdBuffer;
		};

		class VKCommandBundle : public CommandBundle
		{
		public:
			virtual void Reset() override;
			virtual void Close() override;

			virtual void SetPipelineLayout(const PipelineLayout * pPipelineLayout) override;
			virtual void SetPipelineState(const PipelineState * pPipelineState) override;

			virtual void SetPrimitiveTopology(PrimitiveTopology primitiveTopology) override;
			virtual void SetVertexBuffers(UINT start, UINT nViews, Buffer * const * ppBuffers) override;

			virtual void SetBlendFactor(const FLOAT blendFactors[4]) override;
			virtual void SetStencilRefValue(UINT stencilRefValue) override;

			virtual void DrawInstanced(UINT verticesPerInstance, UINT nInstances, UINT vertexStart, UINT instanceStart) override;

			vk::CommandBuffer cmdBuffer;
		};
	}
}