#pragma once

#include "base\src\HGResources.h"
#include "HGVK.h"

namespace hg
{
	namespace Graphics
	{
		class VKBuffer : public Buffer
		{
		public:
			vk::Buffer handle;
			vk::BufferView view;
			vk::DeviceMemory memory;
		};

		class VKTexture : public Texture
		{
		public:
			vk::Image handle;
			vk::ImageView view;
			vk::DeviceMemory memory;
		};

		class VKRenderTarget : public RenderTarget
		{
		public:
			vk::Image handle;
			vk::ImageView view;
			vk::DeviceMemory memory;

			UINT attachmentIdx;
		};

		class VKRenderSubpass : public RenderSubpass
		{
		public:
			vk::SubpassDescription desc;
			
			UINT subpassIdx;
			tinystl::vector<vk::AttachmentReference> inputRef;
			tinystl::vector<vk::AttachmentReference> outputRef;
			tinystl::vector<vk::AttachmentReference> resolveRef;
			tinystl::vector<uint32_t> preservedRef;
			vk::AttachmentReference depthRef;
			tinystl::vector<vk::SubpassDependency> dependencies;
		};

		class VKSampler : public Sampler
		{
		public:
			vk::Sampler handle;
		};

		class VKRenderPass : public RenderPass
		{
		public:
			vk::RenderPass handle;
			
			UINT renderPassIdx;
			UINT framebufferWidth;
			UINT framebufferHeight;
			float framebufferClearColor[4];

			tinystl::vector<vk::Framebuffer> framebuffers;
			tinystl::vector<vk::SubpassDescription> subpasses;
			tinystl::vector<vk::SubpassDependency> subpassesDependencies;

		};
	}
}