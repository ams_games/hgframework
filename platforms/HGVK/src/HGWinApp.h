#pragma once

#include "base\src\HGBaseApp.h"

#include "HGVK.h"
#include "HGVKRenderer.h"

namespace hg
{
	class WinApp : public BaseApp
	{
	public:
		WinApp(const UINT & width, const UINT & height, const std::wstring &name);

		// Handles setters/getters
		void SetWindow(HWND hWindow) { m_hWindow = hWindow; }
		HWND GetWindow() { return m_hWindow; }
		void SetInstance(HINSTANCE hInstance) { m_hInstance = hInstance; }
		HINSTANCE GetInstance() { return m_hInstance; }

		// Command line args parsing.
		virtual void ParseCmdLineArgs(const char * cmds);

		// Cursor handling.
		virtual void GetWindowCenter(UINT& centerX, UINT& centerY) override;
		virtual void TrapCursor() override;
		virtual void FreeCursor() override;

		// Input handling.
		virtual bool IsKeyPressed(const UINT8 &key) override;

	private:
		// Platform initialization/destruction.
		virtual bool initPlatform() override;
		virtual bool destroyPlatform() override;

		HWND m_hWindow;
		HINSTANCE m_hInstance;
	};
}