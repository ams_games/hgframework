#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

// Input
layout (location = 0) in vec4 fragColor;

// Output
layout (location = 0) out vec4 outColor;

void main() {
#ifdef DEBUG
	outColor = 1.0f - fragColor;
#else
	outColor = fragColor;
#endif
}