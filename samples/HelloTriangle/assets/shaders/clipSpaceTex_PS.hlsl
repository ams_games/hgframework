struct PSInput
{
	float4 position : SV_POSITION;
	float2 uvs : TEXCOORD0;
};

Texture2D base;
SamplerState texSampler;

float4 main(PSInput input) : SV_TARGET
{
	float4 color = base.Sample(texSampler, input.uvs);
	//float4 color = float4(input.uvs, 0, 1);

#ifdef DEBUG
	return 1.0f - color;
#else
	return color;
#endif
}