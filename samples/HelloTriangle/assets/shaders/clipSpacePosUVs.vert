#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#include "HGShaderCommon.incl"

// Input
layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec2 inUVs;

// Resources
layout (set = FRAME_SET, binding = VERT_UNIFORM_BINDING(0)) uniform frameData
{
	mat4 MVPMatrix;
	bool isOrtho;
};

// Output
layout(location = 0) out vec2 fragUVs;

void main() {
	gl_Position = MVPMatrix * inPosition;
	if(isOrtho)
	{
		gl_Position.z /= gl_Position.w;
		gl_Position.w = 1.0f;
	}
	gl_Position.y *= -1.0f;

	fragUVs = inUVs;
}