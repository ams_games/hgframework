#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#include "HGShaderCommon.incl"

// Input
layout (location = 0) in vec2 fragUVs;

// Resources
layout (set = STATIC_SET, binding = FRAG_SAMPLER2D_BINDING(0)) uniform sampler2D base;

// Output
layout (location = 0) out vec4 outColor;

void main() {
	vec4 color = texture(base, fragUVs);

#ifdef DEBUG
	outColor = 1.0f - color;
#else
	outColor = color;
#endif
}