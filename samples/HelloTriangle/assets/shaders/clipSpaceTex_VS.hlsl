struct PSInput
{
	float4 position : SV_POSITION;
	float2 uvs : TEXCOORD0;
};

PSInput main(float4 position : POSITION, float4 uvs : TEXCOORD0)
{
	PSInput result;

	result.position = position;
	result.uvs = uvs;

	return result;
}
