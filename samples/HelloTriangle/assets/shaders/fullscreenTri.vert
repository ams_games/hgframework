#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

// Output
layout(location = 0) out vec2 fragUVs;

void main() {
	gl_Position = vec4((gl_VertexIndex  == 2) ? 3.0f : -1.0f, (gl_VertexIndex == 0) ? -3.0f : 1.0f, 0.0f, 1.0f);
	fragUVs = gl_Position.xy * vec2(0.5f, 0.5f) + vec2(0.5f, 0.5f);
}