#pragma once

#if defined(DX12)
#include "platforms\HGDX12\src\HGWinApp.h"
#elif defined(VK)
#include "platforms\HGVK\src\HGWinApp.h"
#endif

using namespace hg;

#if defined(_WIN32)
class Application : public WinApp
{
	typedef	WinApp	Parent;
#endif

public:
	Application(const UINT & width, const UINT & height, const std::wstring &name) : Parent(width, height, name) {};

	void ParseCmdLineArgs(const char * cmds);

	void ResizeWindow() override;
	void OnKeyUp(const UINT8 &key) override;
	void OnMouseMove(const UINT &xCoord, const UINT &yCoord) override;
	void OnMouseButton(const UINT &xCoord, const UINT &yCoord, const Input::MouseButton &mouseButton, const bool &buttonDown) override;
	void OnMouseWheel(const UINT &xCoord, const UINT &yCoord, const float &wheelDelta) override;

private:
	bool start() override;
	bool update() override;
	bool render() override;
	bool end() override;

	// GRAPHICS //
	struct Vertex
	{
		mathfu::Vector<float, 3> position;
		mathfu::Vector<float, 2> uvs;
	};

	struct FrameData
	{
		mathfu::Matrix<float, 4, 4> MVPMatrix;
		bool isOrtho;
	} m_frameData;

	struct Camera
	{
	public:
		Camera() {};
		Camera(const float& fov, const float& aspectRatio, const float& nearClippingDistance,
			const mathfu::Vector<float, 3>& position)
		{
			this->fov = fov;
			this->aspectRatio = aspectRatio;
			this->nearClippingDistance = nearClippingDistance;
			this->position = position;
			this->rotation = mathfu::Quaternion<float>::LookAt(mathfu::kAxisZ3f, mathfu::kAxisY3f);
			this->hRot = 0.0f;
			this->vRot = 0.0f;
		}

		void MoveSideways(const float& movementFactor) { position += rotation * mathfu::kAxisX3f * movementFactor; }
		void MoveUpwards(const float& movementFactor) { position += rotation * mathfu::kAxisY3f * movementFactor; }
		void MoveForward(const float& movementFactor) { position += rotation * mathfu::kAxisZ3f * movementFactor;; }

		void Rotate(const float& hDegrees, const float& vDegrees)
		{
			hRot += hDegrees;
			vRot += vDegrees;

			rotation = mathfu::Quaternion<float>::FromEulerAngles(mathfu::Vector<float, 3>(
				static_cast<float>(DEGREES_TO_RADIANS(vRot)),
				static_cast<float>(DEGREES_TO_RADIANS(hRot)),
				0.0f)) * mathfu::Quaternion<float>::LookAt(mathfu::kAxisZ3f, mathfu::kAxisY3f);
		}

		void Zoom(const float& fovDegrees)
		{
			fov -= static_cast<float>(DEGREES_TO_RADIANS(fovDegrees));
			fov = mathfu::Clamp(fov, 0.01f, static_cast<float>(PI) - 0.01f);
		}

		void SetAspectRatio(const float& aspect) { aspectRatio = aspect; }
		void SetNearClippingDistance(const float& distance) { nearClippingDistance = distance; }

		mathfu::Matrix<float, 4, 4> WorldToCamera(const mathfu::Matrix<float, 4, 4>& transformMatrix)
		{
			const mathfu::Matrix<float, 4, 4> cameraTransform =
				Math::GetTranslationMatrix(position.x, position.y, position.z) *
				rotation.ToMatrix4();
			mathfu::Matrix<float, 4, 4> inverseCameraTransform = cameraTransform.Inverse();;

			return Math::GetReversedZProjectionMatrix(fov, aspectRatio, nearClippingDistance) *
				inverseCameraTransform *
				transformMatrix;
		}

	private:
		float fov;
		float aspectRatio;
		float nearClippingDistance;

		mathfu::Vector<float, 3> position;
		mathfu::Quaternion<float> rotation;
		float hRot, vRot;
	} m_camera;

	Graphics::RenderSubpass* m_quadSubpass;
	Graphics::RenderPass* m_quadRenderPass;
	Graphics::CommandRecorder* m_commandRecorder;

	Graphics::Sampler* m_sampler;

	Graphics::Shader* m_quadShader;
	Graphics::PipelineLayout* m_quadPipelineLayout;
	Graphics::PipelineState* m_quadPipelineState;

	Graphics::Shader* m_blitShader;
	Graphics::PipelineLayout* m_blitPipelineLayout;
	Graphics::PipelineState* m_blitPipelineState;

	Graphics::RenderTarget* m_quadRT;
	Graphics::RenderTarget* m_depthRT;
	Graphics::Buffer* m_quadVertexBuffer;
	Graphics::Buffer* m_quadIndexBuffer;
	Graphics::Buffer* m_frameUniformBuffer;
	Graphics::Texture* m_quadTexture;

	bool m_ortho;
};