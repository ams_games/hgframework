#include "Application.h"

BaseApp * application = new Application(960, 540, L"HelloTriangle!");

void Application::ParseCmdLineArgs(const char * cmds)
{
	Parent::ParseCmdLineArgs(cmds);
}

void Application::ResizeWindow()
{
	if (m_running)
	{
		m_renderer->ResizeMainFramebuffer();
		m_renderer->DestroyPipelineState(m_blitPipelineState);
		m_blitPipelineState = m_renderer->CreatePipelineState(m_blitShader, m_blitPipelineLayout, m_renderer->GetMainRenderPass(), 0);

		m_camera.SetAspectRatio(m_aspectRatio);
	}
}

void Application::OnKeyUp(const UINT8 & key)
{
	if (key == Input::KEY_F1) m_ortho = !m_ortho;
}

void Application::OnMouseMove(const UINT & xCoord, const UINT & yCoord)
{
	// Calculate camera rotation if cursor is trapped.
	UINT centerX, centerY;
	GetWindowCenter(centerX, centerY);

	float xDelta = (static_cast<float>(xCoord) - static_cast<float>(centerX)) * 0.1f;
	float yDelta = (static_cast<float>(yCoord) - static_cast<float>(centerY)) * 0.1f * m_aspectRatio;

	m_camera.Rotate(xDelta, yDelta);
}

void Application::OnMouseButton(const UINT & xCoord, const UINT & yCoord, const Input::MouseButton & mouseButton, const bool & buttonDown)
{
	if (mouseButton == Input::MouseButton::RIGHT_BUTTON && !buttonDown)
	{
		if (m_cursorVisible) TrapCursor();
		else FreeCursor();
	}
}

void Application::OnMouseWheel(const UINT & xCoord, const UINT & yCoord, const float & wheelDelta)
{
	m_camera.Zoom(wheelDelta * 10.0f);
}

bool Application::start()
{
	// Create the quad render pass.
	float rtClearColor[4] = { 0.4f, 0.2f, 1.0f, 1.0f };
	m_quadRT = m_renderer->CreateRenderTarget(0u, 3840u, 2160u, Graphics::TextureFormat::R8G8B8A8_SRGB, rtClearColor,
		Graphics::SampleCount::SC_1, Graphics::TextureDimensions::TD_2D, 1, 1, true);
	m_depthRT = m_renderer->CreateRenderTarget(0u, 3840u, 2160u, Graphics::TextureFormat::D32, rtClearColor,
		Graphics::SampleCount::SC_1, Graphics::TextureDimensions::TD_2D, 1, 1, false);
	m_quadSubpass = m_renderer->CreateRenderSubpass(Graphics::PipelineType::PT_GRAPHICS, 0u,
		0u, nullptr, 1u, &m_quadRT, 0u, true, m_depthRT, 0u, nullptr, 0u, nullptr);
	m_quadRenderPass = m_renderer->CreateRenderPass(3840u, 2160u, rtClearColor, 1, &m_quadSubpass);

	// Command recorder creation.
	m_commandRecorder = m_renderer->CreateCommandRecorder(Graphics::CommandRecorderType::CRT_DIRECT);

	// Create a texture sampler.
	m_sampler = m_renderer->CreateSampler(
		Graphics::FilteringMode::LINEAR, Graphics::FilteringMode::LINEAR, Graphics::FilteringMode::LINEAR,
		Graphics::TextureAddressMode::REPEAT, Graphics::TextureAddressMode::REPEAT, Graphics::TextureAddressMode::REPEAT);

	// Pipeline initialization.
	m_quadShader = m_renderer->LoadShader(L"clipSpacePosUVs", L"", L"", L"", L"simpleTex", L"");
	m_quadPipelineLayout = m_renderer->CreatePipelineLayout(m_quadShader);
	m_quadPipelineState = m_renderer->CreatePipelineState(m_quadShader, m_quadPipelineLayout, m_quadRenderPass, 0, Graphics::CullMode::BACK);

	m_blitShader = m_renderer->LoadShader(L"fullscreenTri", L"", L"", L"", L"simpleTex", L"");
	m_blitPipelineLayout = m_renderer->CreatePipelineLayout(m_blitShader);
	m_blitPipelineState = m_renderer->CreatePipelineState(m_blitShader, m_blitPipelineLayout, m_renderer->GetMainRenderPass(), 0, Graphics::CullMode::NONE);

	// Resource loading/creation.
	Vertex quadVertices[8] =
	{
		{ mathfu::Vector<float, 3>(0.5f, 0.5f, -0.5f), mathfu::Vector<float, 2>(1.0f, 1.0f) },
		{ mathfu::Vector<float, 3>(0.5f, -0.5f, -0.5f), mathfu::Vector<float, 2>(1.0f, 0.0f) },
		{ mathfu::Vector<float, 3>(-0.5f, -0.5f, -0.5f), mathfu::Vector<float, 2>(0.0f, 0.0f) },
		{ mathfu::Vector<float, 3>(-0.5f, 0.5f, -0.5f), mathfu::Vector<float, 2>(0.0f, 1.0f) },
		{ mathfu::Vector<float, 3>(0.5f, 0.5f, 0.5f), mathfu::Vector<float, 2>(0.0f, 1.0f) },
		{ mathfu::Vector<float, 3>(0.5f, -0.5f, 0.5f), mathfu::Vector<float, 2>(0.0f, 0.0f) },
		{ mathfu::Vector<float, 3>(-0.5f, -0.5f, 0.5f), mathfu::Vector<float, 2>(1.0f, 0.0f) },
		{ mathfu::Vector<float, 3>(-0.5f, 0.5f, 0.5f), mathfu::Vector<float, 2>(1.0f, 1.0f) }
	};
	m_quadVertexBuffer = m_renderer->CreateBuffer(sizeof(quadVertices), sizeof(Vertex), Graphics::BufferUsage::BU_VERTEX_BUFFER, quadVertices);

	UINT quadIndices[36] =
	{
		0, 1, 2,
		2, 3, 0,
		4, 5, 1,
		1, 0, 4,
		7, 6, 5,
		5, 4, 7,
		3, 2, 6,
		6, 7, 3,
		4, 0, 3,
		3, 7, 4,
		1, 5, 6,
		6, 2, 1
	};
	m_quadIndexBuffer = m_renderer->CreateBuffer(sizeof(quadIndices), sizeof(UINT), Graphics::BufferUsage::BU_INDEX_BUFFER, quadIndices);

	m_frameUniformBuffer = m_renderer->CreateBuffer(sizeof(FrameData), sizeof(FrameData), Graphics::BufferUsage::BU_UNIFORM_BUFFER);

	m_quadTexture = m_renderer->LoadTexture(L"UV Test.png");

	// Static resource binding.
	m_renderer->BindTexture("base", m_quadTexture, m_sampler, m_quadPipelineLayout);
	m_renderer->BindTexture("base", m_quadRT, m_sampler, m_blitPipelineLayout);

	// Camera creation.
	m_camera = Camera(static_cast<float>(DEGREES_TO_RADIANS(90.0)), m_aspectRatio, 0.01f, mathfu::Vector<float, 3>(0.0f, 0.0f, -1.5f));

	return true;
}

bool Application::update()
{
	// Input processing.
	{
		if (IsKeyPressed(Input::KEY_W)) m_camera.MoveForward(static_cast<float>(1.5 * GetFrameDelta()));
		else if (IsKeyPressed(Input::KEY_S)) m_camera.MoveForward(-static_cast<float>(1.5 * GetFrameDelta()));

		if (IsKeyPressed(Input::KEY_D)) m_camera.MoveSideways(static_cast<float>(1.5 * GetFrameDelta()));
		else if (IsKeyPressed(Input::KEY_A)) m_camera.MoveSideways(-static_cast<float>(1.5 * GetFrameDelta()));

		if (IsKeyPressed(Input::KEY_E)) m_camera.MoveUpwards(static_cast<float>(1.5 * GetFrameDelta()));
		else if (IsKeyPressed(Input::KEY_Q)) m_camera.MoveUpwards(-static_cast<float>(1.5 * GetFrameDelta()));
	}
	
	// Per-frame resource binding.
	float rotation = static_cast<float>(DEGREES_TO_RADIANS(GetApplicationTime() * (360.0 / 10.0)));
	m_frameData.MVPMatrix = m_camera.WorldToCamera(
		Math::GetTransformationMatrix(
		0.0f, 0.0f, 0.0f,
		1.0f, 1.0f, 1.0f,
		rotation, rotation, rotation));
	m_frameData.isOrtho = m_ortho;

	m_renderer->UpdateBuffer(m_frameUniformBuffer, &m_frameData);
	m_renderer->BindBuffer("frameData", m_frameUniformBuffer, m_quadPipelineLayout);

	return true;
}

bool Application::render()
{
	// Command list recording
	m_commandRecorder->Reset();

	// RenderPass 0 || Subpass 0 : Quad rendering
	m_commandRecorder->BeginRenderPass(m_quadRenderPass);
	Graphics::Viewport viewport = Graphics::Viewport(0.0f, 0.0f, 3840, 2160.0f, 0.0f, 1.0f);
	Graphics::Rect rect = Graphics::Rect(0, 0, 3840L, 2160L);
	m_commandRecorder->SetPipelineLayout(m_quadPipelineLayout);
	m_commandRecorder->SetPipelineState(m_quadPipelineState);
	m_commandRecorder->SetViewports(1, &viewport);
	m_commandRecorder->SetScissorRects(1, &rect);
	m_commandRecorder->SetPrimitiveTopology(Graphics::PrimitiveTopology::PT_TRIANGLELIST); // NOTE : This doesn't do anything on Vulkan.
	m_commandRecorder->SetVertexBuffers(0, 1, &m_quadVertexBuffer);
	m_commandRecorder->SetIndexBuffer(m_quadIndexBuffer);
	m_commandRecorder->DrawIndexed(36, 1, 0, 0, 0);
	m_commandRecorder->EndRenderPass();

	// RenderPass 1 || Subpass 0 : Blit to surface
	m_commandRecorder->BeginMainRenderPass(m_renderer->GetMainRenderPass(), m_renderer->GetMainFramebufferIdx());
	Graphics::Viewport fullscreenViewport = Graphics::Viewport(0.0f, 0.0f, static_cast<float>(m_windowWidth), static_cast<float>(m_windowHeight), 0.0f, 1.0f);
	Graphics::Rect fullscreenRect = Graphics::Rect(0, 0, static_cast<LONG>(m_windowWidth), static_cast<LONG>(m_windowHeight));
	m_commandRecorder->SetPipelineLayout(m_blitPipelineLayout);
	m_commandRecorder->SetPipelineState(m_blitPipelineState);
	m_commandRecorder->SetViewports(1, &fullscreenViewport);
	m_commandRecorder->SetScissorRects(1, &fullscreenRect);
	m_commandRecorder->Draw(3, 1, 0, 0);
	m_commandRecorder->EndRenderPass();

	// Close the command list
	m_commandRecorder->Close();

	// Execute the command list.
	Graphics::CommandRecorder* ppCommandRecorders[] = { m_commandRecorder };
	m_renderer->ExecuteCommands(_countof(ppCommandRecorders), ppCommandRecorders);

	// Present should go after finish frame.
	m_renderer->Present();

	// Wait for command list execution.
	m_renderer->FinishFrame();

	return true;
}

bool Application::end()
{
	// Destroy application elements.
	m_renderer->DestroyTexture(m_quadTexture);
	m_renderer->DestroyBuffer(m_quadIndexBuffer);
	m_renderer->DestroyBuffer(m_frameUniformBuffer);
	m_renderer->DestroyBuffer(m_quadVertexBuffer);

	m_renderer->DestroyPipelineState(m_blitPipelineState);
	m_renderer->DestroyPipelineLayout(m_blitPipelineLayout);
	m_renderer->DestroyShader(m_blitShader);

	m_renderer->DestroyPipelineState(m_quadPipelineState);
	m_renderer->DestroyPipelineLayout(m_quadPipelineLayout);
	m_renderer->DestroyShader(m_quadShader);

	m_renderer->DestroySampler(m_sampler);
	m_renderer->DestroyCommandRecorder(m_commandRecorder);
	m_renderer->DestroyRenderPass(m_quadRenderPass);
	m_renderer->DestroyRenderSubpass(m_quadSubpass);
	m_renderer->DestroyRenderTarget(m_depthRT);
	m_renderer->DestroyRenderTarget(m_quadRT);

	return true;
}
