# README #

Repository for the HgFramework (Mercury Framework), a rendering framework developed by Andr�s Valencia T�llez as a foundational base for future DX12 / Vulkan / Metal2 demos.

## Third-party open-source libraries ##

### Rendering Framework ###

* [TinySTL](https://github.com/mendsley/tinystl)
* [Gainput](https://github.com/jkuhlmann/gainput)
* [MathFu](https://github.com/google/mathfu)
* [STB](https://github.com/nothings/stb)
* [TinyEXR](https://github.com/syoyo/tinyexr)
* [Font Stash](https://github.com/memononen/fontstash)
* [Assimp](https://github.com/assimp/assimp)
* [SPIRV-Cross](https://github.com/KhronosGroup/SPIRV-Cross)
* [LibAV](https://www.libav.org/)